from tps import ThinPlateSpline
import numpy as np
import cv2
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from scipy.interpolate import Rbf

# Semantics :- input_nodes_table has node_id and corresponding original 
# geom mapped_nodes_table has node_id and final geom of some of the nodes

def transform_points(psql_conn, input_nodes_table, mapped_nodes_table, output_nodes_table, method='polynomial', srid = 32643, degree = 1):
    sql = f"""
        select 
            input.node_id,
            st_x(input.geom),
            st_y(input.geom),
            mapped.node_id,
            st_x(mapped.geom),
            st_y(mapped.geom)
        from
            {input_nodes_table} as input
        left join
            {mapped_nodes_table} as mapped
            on input.node_id = mapped.node_id
        ;
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
        mapped_points = curr.fetchall()
        
    transformation_input = np.array([(i[1],i[2]) for i in mapped_points if i[3] is not None], dtype=np.float64)
    transformation_output = np.array([(i[4],i[5]) for i in mapped_points if i[3] is not None], dtype=np.float64)
    
    points_to_transform = np.array([(i[1],i[2]) for i in mapped_points])
    
    if method=='polynomial':
        transformed_points = polynomial(transformation_input, transformation_output, points_to_transform, degree = degree)
    elif method=='projective':
        transformed_points = projective(transformation_input, transformation_output, points_to_transform)
    elif method=='spline':
        transformed_points = spline(transformation_input, transformation_output, points_to_transform)
    elif method == 'spline2':
        transformed_points = spline2(transformation_input, transformation_output, points_to_transform)
    elif method == 'spline4':
        transformed_points = spline4(transformation_input, transformation_output, points_to_transform)
    elif method == 'spline3':
        transformed_points = spline3(transformation_input, transformation_output, points_to_transform)
    else:
        print(f"Transformation {method} not found !!!")
        return
    
    values_to_insert = [f"({id[0]},st_point({coords[0]}, {coords[1]}, {srid}))" for id,coords in zip(mapped_points,transformed_points)]    
    values_query = ",".join(values_to_insert)
    
    sql = f"""
        drop table if exists {output_nodes_table};
        create table {output_nodes_table} (
            node_id integer,
            geom geometry(Point, {srid})
        );
        insert into {output_nodes_table} (node_id, geom)
        values {values_query};
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)

def polynomial(transformation_input, transformation_output, points_to_transform, degree = 1):
    
    poly = PolynomialFeatures(degree=degree)
    in_features = poly.fit_transform(transformation_input)
    model = LinearRegression()
    model.fit(in_features, transformation_output)
        
    transformed_points = model.predict(poly.transform(points_to_transform))
    
    return transformed_points

def projective(transformation_input, transformation_output, points_to_transform):
    
    TransformationMatrix, _ = cv2.findHomography(transformation_input, transformation_output)

    transformed_points = cv2.perspectiveTransform(points_to_transform.reshape(-1, 1, 2), TransformationMatrix)
    transformed_points = [(i[0][0],i[0][1]) for i in transformed_points]
    
    return transformed_points
        
def spline(transformation_input, transformation_output, points_to_transform):
    
    tps_model = ThinPlateSpline(alpha = 0.0)
    tps_model.fit(transformation_input,transformation_output)
    transformed_points = tps_model.transform(points_to_transform)    
    
    return transformed_points

def spline2(transformation_input, transformation_output, points_to_transform):

    rbf_model = Rbf(transformation_input, transformation_output, function='thin_plate', smooth=0)
    
    transformed_points = rbf_model(points_to_transform)
    
    return transformed_points

def spline3(transformation_input, transformation_output, points_to_transform):
    tps_model = ThinPlateSpline(alpha = 100000.0)
    tps_model.fit(transformation_input,transformation_output)
    transformed_points = tps_model.transform(points_to_transform)    
    
    return transformed_points

def spline4(transformation_input, transformation_output, points_to_transform):
    common_points = set(map(tuple, transformation_input)).intersection(map(tuple, points_to_transform))
    common_points = list(common_points)
    transformed_points_spline3 = spline3(transformation_input, transformation_output, points_to_transform)
    
    transformed_points = transformed_points_spline3.copy()
    for point_index, point in enumerate(points_to_transform):
        if tuple(point) in common_points:
            index = transformation_input.tolist().index(list(point))
            transformed_points[point_index] = transformation_output[index]
    
    return transformed_points
