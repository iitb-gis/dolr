from .postgres_utils import *

def get_geom_type(psql_conn, table):
    sql = f"""
        select geometrytype(geom) as geometry_type
        from {table}
        limit 1;
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
        type = curr.fetchone()
        if type is None:
            print("ERROR")
            exit()
        
    return type[0]
    

def create_topo(psql_conn, schema, topo_schema, input_table, tol=0, srid=32643, simplify_tol = 0, seg=True):
    
    type = get_geom_type(psql_conn, schema+'.'+input_table)
    
    comment = "" if check_schema_exists(psql_conn, topo_schema) else "--"
    
    sql=f"""
        {comment} select DropTopology('{topo_schema}');
        with topo_id as (
            select
                topology_id
            from
                topology.layer
            where
                schema_name = '{schema}'
                and
                table_name = '{input_table}_t'
                and
                feature_column = 'topo'
        ),
        topo_name as (
            select 
                name
            from
                topology.topology as t,
                topo_id as tid 
            where
                t.id = tid.topology_id
            limit 1
        )
        select DropTopology(name) from topo_name;
        select CreateTopology('{topo_schema}', {srid}, {tol});
        
        drop table if exists {schema}.{input_table}_t;
        create table {schema}.{input_table}_t as table {schema}.{input_table};
        
        select AddTopoGeometryColumn('{topo_schema}', '{schema}', '{input_table}_t','topo', '{type}');
        
        update {schema}.{input_table}_t
        set topo = totopogeom(geom,'{topo_schema}',layer_id(findlayer('{schema}','{input_table}_t','topo')));

        update {topo_schema}.edge_data 
        set geom = coalesce(st_simplify(geom, {simplify_tol}),geom);

        --with points as (
        --    select
        --        (st_dumppoints(geom)).geom as geom
        --    from 
        --        {topo_schema}.edge_data
        --) 
        --select TopoGeo_AddPoint('{topo_schema}',geom, {tol}) from points;
    """
    
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
        
    if seg:
        segmentize(psql_conn, topo_schema, tol)
        
def segmentize(psql_conn, topo_name, seg_tol, seg_length = 10000):
    sql_query=f"""
        with edges as (
            select edge_id, start_node, end_node, geom from {topo_name}.edge_data
        ),
        boundary as (
            select
                (st_dumppoints(st_segmentize(geom, {seg_length}))).geom as point
            from
                edges
        )
        
        select topogeo_addpoint('{topo_name}', point, {seg_tol}) from boundary;
    """
    
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql_query)

def polygonize_topo(psql_conn, schema, topo_name, output):
    sql_query=f"""
        drop table if exists {schema}.{output};
        create table {schema}.{output} as 
        with edges as 
            (
                select 
                    st_collect(geom) as geom
                from
                    {topo_name}.edge_data
            )
        select 
            st_multi((st_dump(st_polygonize(geom))).geom) as geom
        from 
            edges;
            
        alter table {schema}.{output}
        add column gid serial;
            
    """

    with psql_conn.connection().cursor() as curr:
        curr.execute(sql_query)
        

def update_topology(psql_conn, topo, updated_nodes_table, geom_col = "geom"):
    sql = f"""
        with moved_nodes as (
            update {topo}.node as nodes
            set geom = updated_nodes.{geom_col}
            from 
                {updated_nodes_table} as updated_nodes
            where 
                nodes.node_id = updated_nodes.node_id
            returning 
                nodes.node_id, 
                nodes.geom
        ),
        edge_ends as (
            select
                e.edge_id as edge_id,
                mn_start.node_id as start_node_id,
                mn_start.geom as start_node_geom,
                mn_end.node_id as end_node_id,
                mn_end.geom as end_node_geom
            from
                {topo}.edge_data as e
            left join
                moved_nodes as mn_start
                on e.start_node = mn_start.node_id
            left join
                moved_nodes as mn_end
                on e.end_node = mn_end.node_id
        )
        update {topo}.edge_data as e
        set geom = 
            st_setpoint(
                st_setpoint(
                    e.geom, 
                    0, 
                    coalesce(
                        ends.start_node_geom, 
                        st_pointn(e.geom, 1)
                    )
                ),
                -1, 
                coalesce(
                    ends.end_node_geom, 
                    st_pointn(e.geom, -1)
                )
            )
        from edge_ends as ends
        where
            ends.edge_id = e.edge_id
        ;
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)

def add_face_ids(psql_conn, input_table, 
                 topogeom_column_name = 'topo', face_id_column_name = 'face_id'):
    # add_column(psql_conn, f'{input_table}_t', face_id_column_name, 'integer')
    sql = f"""
        alter table {input_table}_t
        add column if not exists {face_id_column_name} integer;
        update {input_table}_t
        set {face_id_column_name} = (
            select 
                (gettopogeomelements({topogeom_column_name}))[1] 
            limit 1
        );
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
    
def get_geom_from_face_ids(psql_conn, topo, input_table, output_table, 
                           face_id_column = 'face_id', drop_topo_column = True):
    sql = f"""
        drop table if exists {output_table};
        create table {output_table} as table {input_table}_t;
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
    
    if drop_topo_column:
        sql = f"""
            alter table {output_table}
            drop column topo;
        """
        with psql_conn.connection().cursor() as curr:
            curr.execute(sql)
    
    sql = f"""
        update {output_table}
        set geom = st_multi(st_getfacegeometry('{topo}',{face_id_column}));
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)

def get_geom_from_topogeom(psql_conn, topo, input_table, output_table, topogeom_column = 'topo'):
    
    add_face_ids(psql_conn, input_table, topogeom_column, 'face_id')
    
    get_geom_from_face_ids(psql_conn, topo, input_table, output_table, 'face_id')
    
# schema is not used here, apart from creating a temporary table in it
def get_updated_faces_from_nodes(psql_conn, topo, schema, input_table, output_table, 
                                 updated_nodes_table, topogeom_column = 'topo'):
    sql = f"""
        drop table if exists {schema}.temp_topo_nodes;
        create table {schema}.temp_topo_nodes as table {topo}.node;
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
    
    update_topology(psql_conn, topo, updated_nodes_table)
    
    get_geom_from_topogeom(psql_conn, topo, input_table, output_table, topogeom_column)
    
    update_topology(psql_conn, topo, schema+'.temp_topo_nodes')