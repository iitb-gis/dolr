import geopandas as gpd
import os
import matplotlib.pyplot as plt
from matplotlib.patches import Patch

def assign_color(value, bins, colors):
    if value is None:
        return '#808080'
    value = abs(value) * 100
    if value < bins[0]:
        return colors[0]
    
    for i in range(len(bins) - 1):
        if bins[i] <= value < bins[i+1]:
            return colors[i+1]
        
    if value >= bins[-1]:
        return colors[-1]
    
    return '#808080'
    
def create_legend_labels(bins):
    legend_labels = [f'< {bins[0]}']
    for i in range(len(bins)-1):
        legend_labels.append(f'{bins[i]} - {bins[i+1]}')
    legend_labels.append(f'> {bins[-1]}')

    return legend_labels

def createplots(psql_conn, village , map, path, column, bins, colors):
    sql = f'''
            select * from {village}.{map}
        '''
    gdf = gpd.GeoDataFrame.from_postgis(sql, psql_conn.connection(), geom_col='geom')
    gdf['color'] = gdf[column].apply(lambda x: assign_color(x, bins, colors))
    # Plot the GeoDataFrame
    fig, ax = plt.subplots(figsize=(10, 8))
    ax.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)
    gdf.plot(ax=ax, color=gdf['color'], edgecolor='black')
    legend_labels = create_legend_labels(bins)
    # Create legend with color boxes and descriptions
    legend_patches = [Patch(color=color, label=label) for color, label in zip(colors + ['#808080'], legend_labels + ['No akarbandh area'])]
    plt.legend(handles=legend_patches, loc='upper left' , bbox_to_anchor=(1, 1))
    plt.suptitle(map)
    plt.title(column)
    plt.savefig(f"{path}/{village}_{column}.png")
    plt.close()

def create_heatmaps(psql_conn, village, path, map, column, bins, colors):
    if not os.path.exists(f"{path}/{village}/{map}"):
        print(f"{path}/{village}/{map}")
        os.makedirs(f"{path}/{village}/{map}", mode=0o777)
    createplots(psql_conn, village, map, f"{path}/{village}/{map}", column, bins, colors)
        
