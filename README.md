Structure:
- All relevant constants are stored in config/, and are accessed via the Config class.
- The config file's default parameters can be replaced by giving arguments during Config init.
- Each file must be runnable on its own, and hence must have a if __name__=="main" section.
- Each module (such as data_loading) must have a main file, with helper files located in scripts/.
- Each main file of a module should have a run function.

Note :- Make sure you are using Bash and have ogr2ogr installed

Installation flow: always at root
1. Create a venv using "python3 -m venv venv"
2. Activate venv usin "source venv/bin/activate"
3. pip3 install -e .

Running the pipeline:
- Configuration
    - Open config/
    - Configure setup.json (village name)
    - Configure psql.json (postgres database configuration)
    - Configure data.json (paths to village folder, farm folder, toggle)
- Prepare data
    - Run python3 src/data_loading/load_data.py
    - If loading to a global farm plot schema at once, pass the flag "-s farm_schema"
- Data validation
    - Run python3 src/data_validation/validate_data.py
- Farm graph
    - Run python3 src/farm_graph/create_graph.py
    - If loading from a global farm plot schema, pass the flag "-s farm_schema"
- Global Georeferencing
    - Run python3 src/georeferencing/georeferencing.py
- Local Georeferencing
    - Run python3 src/jitter_parts/jitter_spline.py
- Local Face Fit
    - Run python3 src/face_fit/refine_map.py
- Output Validation
    - Run python3 src/validation/validate_maps.py
- Save data
    - Run python3 save_data.py



