from utils import *
from config import *
import argparse, ast

def gat_jitter(village):
    config = Config()
    if village != "":
        config.setup_details['setup']['village'] = village
    pgconn = PGConn(config)
    
    return Gat_Jitter(config,pgconn)

class Gat_Jitter:
    def __init__(self,config,psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        
        self.village = self.config.setup_details['setup']['village']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        self.cadastrals = self.config.setup_details['data']['cadastrals_table']

        self.gat_map = self.config.setup_details["gat_jitter"]["gat_map_table"]
        self.gat_map_processed = self.config.setup_details["gat_jitter"]["gat_map_processed_table"]
        self.gat_area = self.config.setup_details["gat_jitter"]["gat_area_table"]
        self.gat_jitter = self.config.setup_details["gat_jitter"]["gat_jitter_table"]
        self.gat_shifted = self.config.setup_details["gat_jitter"]["gat_shifted_table"]
        self.gat_scaled_rotated = self.config.setup_details["gat_jitter"]["gat_scaled_rotated_table"]
        
    def shift_a_to_b(self, a, b, output):
        output_table= self.village + "." + output
        a = self.village + "." + a
        b = self.village + "." + b
        sql = f'''
            drop table if exists {output_table};
            create table {output_table} as table {a};
                
            with delta as (
                select 
                    st_x(st_centroid(st_union(p.geom))) - st_x(st_centroid(st_union(q.geom))) as dx,
                    st_y(st_centroid(st_union(p.geom))) - st_y(st_centroid(st_union(q.geom))) as dy
                from 
                    {b} as p, {a} as q 
            )
            update {output_table} 
            set geom = st_translate(geom,(select dx from delta),(select dy from delta));
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

        
    def run(self):
        bnd1 = self.config.setup_details['gat_jitter']['bnd1']
        bnd1 = ast.literal_eval(bnd1)
        bnd2 = self.config.setup_details['gat_jitter']['bnd2']
        bnd2 = ast.literal_eval(bnd2)
        
        self.shift_a_to_b(self.gat_map_processed, self.cadastrals, self.gat_shifted)
        jitter_fit(self.psql_conn, self.village, self.gat_shifted, self.gat_scaled_rotated, self.cadastrals, 0, bnd1)
        jitter_fit(self.psql_conn, self.village, self.gat_scaled_rotated, self.gat_jitter, self.farmplots, 1, bnd2)

    #     self.setup_validate()
        
    # def setup_validate(self):
    #     add_varp(self.psql_conn, self.schema_name, self.survey_georeferenced, 'varp')
    #     add_farm_intersection(self.psql_conn, self.schema_name, self.survey_georeferenced, self.farmplots, 'farm_intersection')
    #     add_farm_rating(self.psql_conn, self.schema_name, self.survey_georeferenced, self.farmplots, 'farm_rating')
    #     add_shape_index(self.psql_conn, self.schema_name, self.survey_georeferenced, 'shape_index')
        
    
if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    argument = parser.parse_args()
    village = argument.village
    gat = gat_jitter(village)
    gat.run()
    