from config import *
from utils import *
import argparse

def setup_facefit(village=""):
    config = Config()
    
    pgconn = PGConn(config)
    if village != "":    
        config.setup_details['setup']['village'] = village
    
    return Setup_Facefit(config,pgconn)

class Setup_Facefit:
    def __init__(self, config: Config, psql_conn: PGConn):
        self.config = config
        self.psql_conn = psql_conn
        self.village = self.config.setup_details['setup']['village']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        self.srid = config.setup_details['setup']['srid']
        
        self.inp = self.config.setup_details['fbfs']['input_table']
        self.ori = self.config.setup_details['fbfs']['original_faces_table']
        self.nar = self.config.setup_details['fbfs']['narrow_faces_table']
        self.topo = self.village + self.config.setup_details['fbfs']['input_topo_suffix']
        self.nar_mid = self.config.setup_details['fbfs']['narrow_midlines_table']

        self.tol = self.config.setup_details['fbfs']['topo_tol']
        self.seg_tol = self.config.setup_details['fbfs']['seg_tol']
        self.seg_length = self.config.setup_details['fbfs']['seg_length']
        self.nar_face_shp_index_thresh = self.config.setup_details['fbfs']['nar_face_shp_index_thresh']
        self.cleaning_angle_thresh = self.config.setup_details['fbfs']['cleaning_angle_thresh']

        self.covered_nodes = config.setup_details['fbfs']['covered_nodes_table']
        self.best_face_table = self.config.setup_details['fbfs']['best_face_table']
        self.covered_edges = config.setup_details['fbfs']['covered_edges_table']
        self.covered_faces = config.setup_details['fbfs']['covered_faces_table']
        self.visited_faces = config.setup_details['fbfs']['visited_faces_table']
        self.face_node_map = config.setup_details['fbfs']['face_node_map_table']

        self.actual_area_col = config.setup_details['fbfs']['actual_area_column']

        if self.village == "":
            print("ERROR")
            exit()             
            
    def clean_nodes(self):
        theta = int(self.cleaning_angle_thresh)
        b1 = 180-theta
        b2 = 180+theta
        b3 = 360-theta
        b4 = theta
        sql_query = """
            with two_points as (
                with neigh as (
                    select
                        count(edge_id),
                        node_id
                    from
                        {topo_schema}.edge as p,
                        {topo_schema}.node
                    where
                        start_node = node_id
                        or end_node = node_id
                    group by
                        node_id
                )
                select
                    r.node_id, r.geom
                from
                    {topo_schema}.node as r,
                    neigh
                where
                    r.node_id = neigh.node_id
                    and (neigh.count = 2)
            ),
            narrow_faces as (
                select
                    coalesce(
                        st_union(st_makevalid(st_getfacegeometry('{topo_schema}', face_id))),
                        st_geomfromtext('POLYGON EMPTY',{srid})
                    ) as geom
                from
                    {topo_schema}.face
                where
                    face_id>0
                    and
                    st_area(st_makevalid(st_getfacegeometry('{topo_schema}', face_id)))>1
                    and
                    power(st_perimeter(st_makevalid(st_getfacegeometry('{topo_schema}', face_id))),2)/st_area(st_makevalid(st_getfacegeometry('{topo_schema}', face_id))) >= {thresh}
            )

            select
                p.edge_id as e1,
                q.edge_id as e2
            from
                {topo_schema}.edge as p,
                {topo_schema}.edge as q,
                narrow_faces
            where
                p.edge_id != q.edge_id
                and
                not st_isempty(st_intersection(p.geom, q.geom))
                and
                st_intersection(p.geom,q.geom) in (select geom from two_points)
                and
                not st_intersects(st_intersection(p.geom, q.geom), st_buffer(narrow_faces.geom, 5))
                and
                (
                    (degrees(st_angle(p.geom,q.geom)) > {b1}
                    and degrees(st_angle(p.geom,q.geom)) < {b2}) or
                    degrees(st_angle(p.geom,q.geom)) > {b3}
                    or degrees(st_angle(p.geom,q.geom)) < {b4}
                )
            ;
        """.format(topo_schema = self.topo, thresh=self.nar_face_shp_index_thresh, srid=self.srid, b1=b1, b2=b2, b3=b3, b4=b4)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
            all_pairs=curs.fetchall()
        visited=[]
        for pairs in all_pairs:
            if pairs[0] in visited or pairs[1] in visited:
                continue

            sql_query="""select st_newedgeheal('{topo_schema}', {pair1}, {pair2})""".format(
                topo_schema=self.topo, pair1=pairs[0], pair2=pairs[1]
            )

            with self.psql_conn.connection().cursor() as curs:
                curs.execute(sql_query)

            visited.append(pairs[0])
            visited.append(pairs[1])
            
        sql_query="""select 
                        st_changeedgegeom(
                            '{topo_schema}',
                            e.edge_id, 
                            st_makeline(
                                (select geom from {topo_schema}.node where node_id = e.start_node),
                                (select geom from {topo_schema}.node where node_id = e.end_node)
                            )
                        )
                    from 
                        {topo_schema}.edge_data e
                """.format(topo_schema=self.topo)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
    
    def make_faces(self):
        sql_query=f"""
            select polygonize('{self.topo}');
            
            alter table {self.village}.{self.inp}_t
            add column if not exists face_id integer;
            
            update {self.village}.{self.inp}_t
            set face_id = (select (gettopogeomelements(topo))[1] limit 1);

            drop table if exists {self.village}.{self.ori};
            create table {self.village}.{self.ori} as
            
            select
                face_id,
                geom,
                survey_no,
                {self.actual_area_col}
            from
                {self.village}.{self.inp}_t
            where
                power(st_perimeter(geom),2)/st_area(geom) < {self.nar_face_shp_index_thresh}
                and 
                left(survey_no, 1) != 'G'
            ;
            
            
            
            
            drop table if exists {self.village}.{self.nar};
            create table {self.village}.{self.nar} as
            
            with collected_farmplots as (
                select ST_Collect(geom) as geom
                from {self.village}.{self.farmplots}
            )
            select
                a.face_id,
                a.geom,
                a.survey_no,
                a.{self.actual_area_col}
            from
                {self.village}.{self.inp}_t as a
                CROSS JOIN collected_farmplots AS f
            where
                power(st_perimeter(a.geom),2)/st_area(a.geom) >= {self.nar_face_shp_index_thresh}
                or 
                left(a.survey_no, 1) = 'G'
                or
                ST_Area(ST_Intersection(f.geom, a.geom))/ST_Area(a.geom) < 0.5
            ;


            drop table if exists {self.village}.{self.nar_mid};
            create table {self.village}.{self.nar_mid} as

            select
                face_id,
                st_approximatemedialaxis(geom) as geom
            from
                {self.village}.{self.nar}
            where
                st_area(geom)>1
            ;
        """
        
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql_query)
        
        add_column(self.psql_conn, self.village+'.'+self.ori, 'gid','serial')
        add_column(self.psql_conn, self.village+'.'+self.nar, 'gid','serial')
        add_gist_index(self.psql_conn, self.village, self.ori, 'geom')
        add_gist_index(self.psql_conn, self.village, self.nar, 'geom')
        add_varp(self.psql_conn, self.village, self.ori, 'varp')
        add_varp(self.psql_conn, self.village, self.nar, 'varp')

    def run(self):
        create_topo(self.psql_conn, self.village, self.topo, self.inp, self.tol)
        
        segmentize(self.psql_conn, self.topo, self.seg_tol, self.seg_length)
        for _ in range(10):
            self.clean_nodes()
        self.make_faces()
        
        create_nodes_table(self.psql_conn, self.village, self.covered_nodes, self.srid)
        create_edges_table(self.psql_conn, self.village, self.covered_edges, self.srid)
        create_faces_table(self.psql_conn, self.village, self.covered_faces, self.srid)
        create_faces_table(self.psql_conn, self.village, self.best_face_table, self.srid )
        create_faces_table(self.psql_conn, self.village, self.visited_faces, self.srid)
        
        create_face_node_map(self.psql_conn, self.village, self.face_node_map, self.topo)
        
        create_gist_index(self.psql_conn, self.village, self.farmplots, 'geom')
        create_gist_index(self.psql_conn, self.village, self.covered_nodes, 'geom')
        create_gist_index(self.psql_conn, self.village, self.covered_edges, 'geom')
        create_gist_index(self.psql_conn, self.village, self.covered_faces, 'geom')
        create_gist_index(self.psql_conn, self.village, self.visited_faces, 'geom')
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Description for my parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")

    argument = parser.parse_args()
    
    village = argument.village

    fbfs_setup = setup_facefit(village)
    fbfs_setup.run()

