from config import *
from utils import *
import argparse
from itertools import product
import time

def snap_fit(village=""):
    config = Config()
    
    pgconn = PGConn(config)
    if village != "":    
        config.setup_details['setup']['village'] = village
    
    return Snap_Fit(config,pgconn)

class Snap_Fit:
    def __init__(self, config: Config, psql_conn: PGConn):
        self.config = config
        self.psql_conn = psql_conn
        self.village = self.config.setup_details['setup']['village']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        
        self.inp = self.config.setup_details['fbfs']['input_table']
        self.ori = self.config.setup_details['fbfs']['original_faces_table']
        self.nar = self.config.setup_details['fbfs']['narrow_faces_table']
        self.topo = self.village + self.config.setup_details['fbfs']['input_topo_suffix']
        self.nar_mid = self.config.setup_details['fbfs']['narrow_midlines_table']

        self.corner_nodes = self.config.setup_details['fbfs']['corner_nodes']
        self.angle_thresh = self.config.setup_details['fbfs']['corner_nodes_angle_thresh']
        self.survey_no = self.config.setup_details['val']['survey_no_label']
        self.srid = self.config.setup_details['setup']['srid']
        
        self.covered_nodes = config.setup_details['fbfs']['covered_nodes_table']
        self.covered_edges = config.setup_details['fbfs']['covered_edges_table']
        self.covered_faces = config.setup_details['fbfs']['covered_faces_table']
        self.face_node_map = config.setup_details['fbfs']['face_node_map_table']
        
        self.visited_faces = config.setup_details['fbfs']['visited_faces_table']
        
        self.shifted_faces = config.setup_details['fbfs']['shifted_faces_table']
        self.rated_faces = config.setup_details['fbfs']['rated_faces_table']
        
        self.considered_snaps_debug = config.setup_details['fbfs']['considered_snaps_debug']
        self.temp_nodes_geom_table = config.setup_details['fbfs']['temp_nodes_geom_table']
        
        # self.eligible_area = config.setup_details['fbfs']['eligible_area_table']
        # self.snap_buffer_thresh = config.setup_details['fbfs']['snap_buffer_thresh']
        # self.neighbour_faces = config.setup_details['fbfs']['neighbour_faces_table']
        
        self.actual_area_col = config.setup_details['fbfs']['actual_area_column']
        
        if self.village == "":
            print("ERROR")
            exit()
            
    def setup_snap_fit(self):
        # add all field which are to be tracked
        update_visited_faces(self.psql_conn, self.village, self.visited_faces, self.covered_faces)
        sql = f"""
            drop table if exists {self.village}.{self.considered_snaps_debug};
            create table {self.village}.{self.considered_snaps_debug} (
                node_id integer,
                id integer,
                type varchar default null,
                geom geometry(Point, {self.srid})
            );
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        
        add_farm_rating(self.psql_conn, self.village, self.ori, self.farmplots, 'farm_rating')
        get_corner_nodes(self.psql_conn, self.topo, self.village, self.corner_nodes, self.angle_thresh)
        
        # compute_original_metrics()
        # Store it in original_metrics_table
            
    def add_angle_diff(self, schema, table, topo, face_id, input_table, input_table_id_column, input_table_geom_column):
        add_column(self.psql_conn, schema+'.'+table, 'angle_diff', 'float')
        sql = f"""
            with edges as (
                select 
                    t.seq as seq,
                    t.edge_id as edge_id
                from
                    st_getfaceedges('{topo}',{face_id}) as t(seq, edge_id)
                order by
                    t.seq
            ),
            new_edges as (
                select
                    e.seq as seq,
                    e.edge_id as edge_id,
                    st_makeline(
                        start_nodes.{input_table_geom_column}, 
                        end_nodes.{input_table_geom_column}
                    ) as newgeom,
                    ed.geom as oldgeom
                from
                    edges as e
                join
                    {topo}.edge_data as ed
                    on abs(ed.edge_id) = abs(e.edge_id)
                join
                    {schema}.{input_table} as start_nodes
                    on ed.start_node = start_nodes.{input_table_id_column}
                join
                    {schema}.{input_table} as end_nodes
                    on ed.end_node = end_nodes.{input_table_id_column}
                order by
                    e.seq
            ),
            ct as (
                select
                    count(*) as count
                from
                    new_edges
                limit
                    1
            ),
            angle_diff as (
                select
                    max(
                        abs(
                            abs(
                                degrees(st_angle(n1.newgeom, n2.newgeom)) - 180
                            ) - 
                            abs(
                                degrees(st_angle(n1.oldgeom, n2.oldgeom)) - 180
                            )
                        )
                    ) as angle_diff
                from
                    new_edges n1,
                    new_edges n2,
                    ct as c
                where
                    mod(n1.seq, c.count) = mod(n2.seq - 1, c.count)
            )
            update {schema}.{table} as t
            set angle_diff = (
                select 
                    a.angle_diff
                from 
                    angle_diff as a
            );
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        
    def evaluate_face(self, schema, table, farmplots, reference, topo, face_id, input_table, input_table_id_column, input_table_geom_column):
        add_column(self.psql_conn, schema+'.'+table, 'gid', 'serial')
        add_varp(self.psql_conn, schema, table, 'varp')
        add_farm_intersection(self.psql_conn, schema, table, farmplots, 'farm_intersection')
        add_farm_rating(self.psql_conn, schema, table, farmplots, 'farm_rating')
        add_shape_index(self.psql_conn, schema, table, 'shape_index')
        self.add_angle_diff(schema, table, topo, face_id, input_table, input_table_id_column, input_table_geom_column)
            
        sql = f"""
            select
                t.farm_rating as farm_rating,
                t.varp - r.varp as varp_dif,
                abs((st_area(t.geom)/(r.{self.actual_area_col}))-1)*100 as area_dif,
                t.shape_index as shape_index,
                t.angle_diff as angle_diff
            from
                {schema}.{table} as t
            join
                {schema}.{reference} as r
                on t.face_id = r.face_id
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            vals = curr.fetchone()
        
        return vals    
    
    # threshhold and metrics are assumed to be in order : [farm_rating, varp_diff, area_diff, shape_index, angle_diff]
    def compare_metrics(self, metrics, best_metrics,thresholds):
        # compare metrics and best metrics
        # compare (neighbour_metrics - ori_neighbour_metrics)[area_diff] < 2 %
        return metrics[0]>best_metrics[0] and metrics[0]>thresholds[0] and \
            metrics[1]<thresholds[1] and metrics[2]<thresholds[2] and metrics[3]<thresholds[3] and metrics[4]<thresholds[4]
            
    # def compare_metrics(self, metrics, best_metrics,thresholds,temp_updated_faces_table):
    #     # compare metrics and best metrics
    #     # compare (neighbour_metrics - ori_neighbour_metrics)[area_diff] < 2 %
        
    #     sql = f"""
    #         select {thresholds[3]} >= all 
    #         (
    #             select 
    #                 abs((st_area(t.geom)/t.{self.actual_area_col})-1)
    #             from
    #                 {self.village}.{temp_updated_faces_table} as t,
    #                 {self.village}.{self.neighbour_faces} as r
    #             where
    #                 t.face_id = r.face_id
    #         )
    #         ;
    #     """
    #     with self.psql_conn.connection().cursor() as curr:
    #         curr.execute(sql)
    #         v = curr.fetchone()
        
    #     return v[0] and metrics[0]>best_metrics[0] and metrics[0]>thresholds[0] and \
    #         metrics[1]<thresholds[1] and metrics[2]<thresholds[2] and metrics[3]<thresholds[3] and metrics[4]<thresholds[4]
            
    # def add_neighbours(self, face_id):
    #     sql = f"""
    #         drop table if exists {self.village}.{self.neighbour_faces};
    #         create table {self.village}.{self.neighbour_faces} as
    #         with cur_face as (
    #             select
    #                 face_id as face_id,
    #                 geom as geom
    #             from 
    #                 {self.village}.{self.ori}
    #             where
    #                 face_id = {face_id}
    #         )
    #         select
    #             ori.*
    #         from
    #             {self.village}.{self.ori} as ori,
    #             cur_face as c
    #         where
    #             st_intersects(c.geom, ori.geom)
    #             and
    #             ori.face_id != {face_id}
    #         ;
    #     """
    #     with self.psql_conn.connection().cursor() as curr:
    #         curr.execute(sql)
            
    # def get_eligible_area(self, face_id):
    #     buf = int(self.snap_buffer_thresh) + 10
    #     sql = f"""
    #         drop table if exists {self.village}.{self.eligible_area};
    #         create table {self.village}.{self.eligible_area} as            
    #         select
    #             st_buffer(geom, {str(buf)}, 'join=mitre') as geom
    #         from
    #             {self.village}.{self.ori}
    #         where
    #             face_id = {face_id}
    #         ;
            
    #         insert into {self.village}.{self.eligible_area}
    #         select 
    #             geom
    #         from 
    #             {self.village}.{self.neighbour_faces}
    #         ;            
    #     """
    #     with self.psql_conn.connection().cursor() as curr:
    #         curr.execute(sql)
            
    def cover_face(self, face_id, thresholds, layered=False, only_translate=False):
        possible_node_snaps_table = self.config.setup_details['fbfs']['possible_node_snaps']
        two_degree_nodes_map = self.config.setup_details['fbfs']['two_degree_nodes_map']

        # get_eligible_area()
        # You have to update in node selector that the shifted geom should lie in eligible_area
        # self.add_neighbours(face_id)
        # self.get_eligible_area(face_id)
        
        ns = Node_Selector(self.config, self.psql_conn)
        possible_snaps = ns.get_possible_snaps(face_id, self.village+'.'+possible_node_snaps_table,layered=layered, two_degree_nodes_map=self.village+'.'+two_degree_nodes_map, only_translate=only_translate)
        if possible_snaps == None:
            print("can't find nodes")
            print(f"skipping face {face_id}")
            update_visited_faces(self.psql_conn, self.village, self.visited_faces, self.covered_faces, face_id)
            if config.DEBUG_MODE:
                a = input("\nPress Enter to continue to next face: ")
            return
        possible_snap_ids = [i[1] for i in possible_snaps]
        node_ids = [i[0] for i in possible_snaps]
        
        best_metrics = [0, 0, 0, 0, 0]
        best_face_nodes_table = self.config.setup_details['fbfs']['best_nodes_table'] 
        best_face_table = self.config.setup_details['fbfs']['best_face_table']
        temp_face_nodes_table = self.config.setup_details['fbfs']['temp_nodes_table']
        temp_face_table = self.config.setup_details['fbfs']['temp_face_table']
        
        create_nodes_table(self.psql_conn, self.village, best_face_nodes_table, self.srid)
        
        max_iterations = int(self.config.setup_details['fbfs']['max_faces_per_face'])
        iteration_count = 0
        
        for i in product(*possible_snap_ids):
            iteration_count += 1
            print("\n\nIteration Count", iteration_count)
            create_nodes_table(self.psql_conn, self.village, temp_face_nodes_table, self.srid)
            
            for j, node_id in enumerate(node_ids):
                # add in temp_shifted_nodes the geom corresponding to id i[j] for node_id
                sql = f"""
                    with point as (
                        select
                            node_id,
                            geom
                        from
                            {self.village}.{possible_node_snaps_table}
                        where
                            node_id = {node_id}
                            and
                            id = {i[j]}
                    )
                    insert into {self.village}.{temp_face_nodes_table} (node_id, geom)
                    select
                        node_id,
                        geom
                    from
                        point
                    limit
                        1
                    on conflict do nothing;
                """
                with self.psql_conn.connection().cursor() as curr:
                    curr.execute(sql)
            
            st = time.time()
            
            if layered:
                ns.add_unregistered_nodes(face_id, self.village+'.'+two_degree_nodes_map, self.village+'.'+temp_face_nodes_table)
            get_face(self.psql_conn, self.topo, face_id, self.village, temp_face_table, 
                     temp_face_nodes_table, 'node_id', 'geom')
            validity = check_face_valid(self.psql_conn, face_id, self.village, temp_face_table, 
                             self.covered_faces, self.covered_edges)
            
            # update_topo(): Change topogeom of a list of nodes, taken from temp_face_nodes_table
            et = time.time()
            print("Duration of making a face and checking it's validity :-", et-st)
            
            print(f"Face stored in :- '{self.village}.{temp_face_table}'"," Validity:-",validity)
            
            if config.DEBUG_MODE:
                a = input("\nPress Enter to continue : ")
            
            if not validity:
                if iteration_count >= max_iterations:
                    break
                continue
            
            # cur_metrics = self.evaluate_face()
            # neighbour_metrics = []
            # neighbours = get_neighbours(face_id)
            # for face_id in neighbours:
            #   make_face(face_id, topo) # store in temp_table
            #   neighbour_metrics.append(self.evaluate_face(temp_table))
            
            # temp_updated_faces_table = "temp_updated_faces_table"
            # temp_fixed_nodes_table = "temp_fixed_nodes_table" 
            
            # copy_table(self.psql_conn, self.village+'.'+self.covered_nodes ,self.village+'.'+temp_fixed_nodes_table)
            # update_covered_nodes(self.psql_conn, self.village, temp_fixed_nodes_table, temp_face_nodes_table)
            # get_updated_faces_from_nodes(self.psql_conn, self.topo, self.village, self.village+'.'+self.inp, self.village+'.'+temp_updated_faces_table, self.village+'.'+temp_fixed_nodes_table)
            
                       
            metrics = self.evaluate_face(self.village, temp_face_table, self.farmplots, self.ori, self.topo, face_id, temp_face_nodes_table, 'node_id', 'geom')
            print("Current face metrics :-", metrics)
            print("Best face metrics :-", best_metrics)
            if metrics==None or None in metrics:
                if iteration_count >= max_iterations:
                    break
                continue
            
            # ori_metrics, ori_neighbour_metrics = get_original_metrics()
            # self.compare_metrics(metrics, best_metrics, ori_metrics, ori_neighbour_metrics, thresholds)
            if self.compare_metrics(metrics,best_metrics,thresholds):
                copy_table(self.psql_conn, self.village+'.'+temp_face_nodes_table, 
                           self.village+'.'+best_face_nodes_table)
                copy_table(self.psql_conn, self.village+'.'+temp_face_table, 
                           self.village+'.'+best_face_table)
                best_metrics = metrics
                print("Current face metrics are best updating best metrics !!!")
                
            if config.DEBUG_MODE:
                a = input("\nPress Enter to continue : ")
                
            if iteration_count >= max_iterations:
                break
            
        if best_metrics == [0,0,0,0,0]:
            print(f"skipping face {face_id}")
        else:
            if config.DEBUG_MODE or config.INTERACTIVE_MODE:
                sql = f"""
                    drop table if exists {self.village}.temp_interactive_nodes_table;
                    create table {self.village}.temp_interactive_nodes_table 
                    (like {self.village}.{self.covered_nodes} including constraints);
                """
                with self.psql_conn.connection().cursor() as curr:
                    curr.execute(sql)
                
                print("Best face stored in :-", self.village+'.'+best_face_table)
                print("Enter new geom for the node_ids in table :-", self.village+'.temp_interactive_nodes_table')
                input("Press Enter after adding the geoms : ")
                
                copy_table(self.psql_conn, self.village+'.'+best_face_nodes_table, 
                           self.village+'.'+temp_face_nodes_table)
                
                handle_user_input_nodes(self.psql_conn, self.village, self.covered_nodes, 
                                        temp_face_nodes_table, 
                                        'temp_interactive_nodes_table', 
                                        'node_id', 'geom')
                
                get_face(self.psql_conn, self.topo, face_id, self.village, temp_face_table, 
                        temp_face_nodes_table, 'node_id', 'geom')
                validity = check_face_valid(self.psql_conn, face_id, self.village, temp_face_table, 
                                self.covered_faces, self.covered_edges)
                
                if not validity:
                    print("Newly given nodes are invalid, continuing with old nodes")
                
                else:
                    metrics = self.evaluate_face(self.village, temp_face_table, self.farmplots, 
                                                self.ori, self.topo, face_id, temp_face_nodes_table, 
                                                'node_id', 'geom')
                
                    copy_table(self.psql_conn, self.village+'.'+temp_face_nodes_table, 
                            self.village+'.'+best_face_nodes_table)
                    copy_table(self.psql_conn, self.village+'.'+temp_face_table, 
                            self.village+'.'+best_face_table)
                                    
                    print(f"Updated {best_face_nodes_table} with user input, metrics of new face are :-", metrics)               
                
            commit_face(self.psql_conn, self.topo, self.village, self.covered_nodes, self.covered_edges,
                        self.covered_faces, face_id, best_face_table, best_face_nodes_table, 'node_id', 'geom')
            
            # recreate_faces()
            # This function is to get face IDs from topology and store it in a "real-time" face store
            
            
        update_visited_faces(self.psql_conn, self.village, self.visited_faces, self.covered_faces, face_id)
        if config.DEBUG_MODE:
            a = input("\nPress Enter to continue to next face: ")
    
    def finalize_outputs(self, output_table):
        sql = f"""
            drop table if exists {output_table};
            create table {output_table} as 
            select 
                ori.face_id,
                covered.geom,
                covered.order_id,
                ori.survey_no,
                ori.{self.actual_area_col}
            from
                {self.village}.{self.covered_faces} as covered
            join
                {self.village}.{self.ori} as ori
                on covered.face_id = ori.face_id
            ;
            
            insert into {output_table}
            select 
                nar.face_id,
                covered.geom,
                covered.order_id,
                nar.survey_no,
                nar.{self.actual_area_col}
            from
                {self.village}.{self.covered_faces} as covered
            join
                {self.village}.{self.nar} as nar
                on covered.face_id = nar.face_id
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            
    def cover_face_spline(self, face_id, thresholds):
        self.temp_face_table = self.config.setup_details['fbfs']['temp_face_table']
        self.temp_face_nodes_table = self.config.setup_details['fbfs']['temp_nodes_table']
        
        get_nodes_geom(self.psql_conn, self.village, self.topo, self.temp_nodes_geom_table,
                                    self.face_node_map, self.covered_nodes, face_id) 
        sql = f"""
            drop table if exists {self.village}.input_nodes_table_spline;
            create table {self.village}.input_nodes_table_spline as
            select 
                node_id,
                original_geom as geom
            from
                {self.village}.{self.temp_nodes_geom_table}
            ;
            drop table if exists {self.village}.mapped_nodes_table_spline;
            create table {self.village}.mapped_nodes_table_spline as
            select 
                node_id,
                shifted_geom as geom
            from
                {self.village}.{self.temp_nodes_geom_table}
            where
                shifted_geom is not null
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            
        transform_points(self.psql_conn, self.village+'.input_nodes_table_spline', 
                         self.village+'.mapped_nodes_table_spline', 
                         self.village+'.'+self.temp_face_nodes_table, method='spline', srid = self.srid)
        
        sql = f"""
            update {self.village}.{self.temp_face_nodes_table} as a
            set geom = b.shifted_geom
            from 
                {self.village}.{self.temp_nodes_geom_table} as b
            where
                a.node_id = b.node_id
                and
                b.shifted_geom is not null
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        
        get_face(self.psql_conn, self.topo, face_id, self.village, self.temp_face_table, 
                self.temp_face_nodes_table, 'node_id', 'geom')
        validity = check_face_valid(self.psql_conn, face_id, self.village, self.temp_face_table, 
                        self.covered_faces, self.covered_edges)
        
        if not validity:
            print("Newly given nodes are invalid, continuing with old nodes")
        else:
            commit_face(self.psql_conn, self.topo, self.village, self.covered_nodes, self.covered_edges,
            self.covered_faces, face_id, self.temp_face_table, self.temp_face_nodes_table, 'node_id', 'geom')
        
            
        update_visited_faces(self.psql_conn, self.village, self.visited_faces, self.covered_faces, face_id)
        if config.DEBUG_MODE:
            a = input("\nPress Enter to continue to next face: ")
    
    def fix_spline(self, output_table):
        transform_points(self.psql_conn, self.topo+'.node',
                         self.village+'.'+self.covered_nodes, 
                         self.village+'.temp_splined_nodes', method='spline', srid = self.srid)
        get_updated_faces_from_nodes(self.psql_conn, self.topo, self.village, self.village+'.'+self.inp, self.village+'.'+output_table, self.village+'.temp_splined_nodes')
        
    def run(self):
        start = time.time()
        
        print("-----RUNNING SETUP SNAP FIT-----")
        st = time.time()
        self.setup_snap_fit()
        et = time.time()
        print("Duration of Setup :-",et-st)
        
        thresholds = [0.96, 0.8, 10, 500, 60]
        
        print("-----SETTING UP FACE SCHEDULER-----")
        sched = Face_Scheduler(self.config, self.psql_conn)
        
        while True:
            # st = time.time()
            next_face = sched.next_face()
            if next_face == None:
                break
            next_face_id = next_face['face_id']
            # et = time.time()
            # print("Duration of getting next face :-",et-st)
            # if next_face_id == None:
            #     break
            print("\nSelected next face",next_face_id)
            
            # if next_face_id == 49:
            #     config.DEBUG_MODE = True
            # else:
            #     config.DEBUG_MODE = False
            
            self.cover_face(next_face_id, thresholds, layered=True, only_translate=False)
        
        self.finalize_outputs(self.village+"."+self.rated_faces)
        
        # a = input("\nRated faces produced, moving to debug mode, press enter to continue: ")
        # config.DEBUG_MODE = True
        
        create_faces_table(self.psql_conn, self.village, self.visited_faces, self.srid)
        update_visited_faces(self.psql_conn, self.village, self.visited_faces, self.covered_faces)
        
        thresholds = [0, 1.5, 1000, 500, 90]
        
        sched = Face_Scheduler(self.config, self.psql_conn)
        while True:
            # st = time.time()
            next_face = sched.next_face()
            if next_face == None:
                break
            next_face_id = next_face['face_id']
            # et = time.time()
            # print("Duration of getting next face :-",et-st)
            # if next_face_id == None:
            #     break
            print("\nSelected next face",next_face_id)
            
            # if next_face_id == 49:
            #     config.DEBUG_MODE = True
            # else:
            #     config.DEBUG_MODE = False
            
            self.cover_face(next_face_id, thresholds, layered=True, only_translate=True)
        
        self.finalize_outputs(self.village+"."+self.shifted_faces+"_unsplined")
        
        create_faces_table(self.psql_conn, self.village, self.visited_faces, self.srid)
        update_visited_faces(self.psql_conn, self.village, self.visited_faces, self.covered_faces)
        
        # thresholds = [0, 1.5, 1000, 500, 90]
        
        # sched = Face_Scheduler(self.config, self.psql_conn)
        # while True:
        #     # st = time.time()
        #     next_face = sched.next_face()
        #     if next_face == None:
        #         break
        #     next_face_id = next_face['face_id']
        #     # et = time.time()
        #     # print("Duration of getting next face :-",et-st)
        #     # if next_face_id == None:
        #     #     break
        #     print("\nSelected next face",next_face_id)
            
        #     # if next_face_id == 20:
        #     #     config.DEBUG_MODE = True
        #     # else:
        #     #     config.DEBUG_MODE = False
            
        #     self.cover_face_spline(next_face_id, thresholds)
                
        # self.finalize_outputs(self.village+"."+self.shifted_faces)
        
        self.fix_spline(self.shifted_faces)
        
        end = time.time()
        print("Duration of Snap Fit :-",end-start)
    
if __name__ == "__main__":
    from helper_classes import *
    parser = argparse.ArgumentParser(description="Description for my parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")

    argument = parser.parse_args()
    
    village = argument.village

    sf = snap_fit(village)
    sf.run()

else:
    from .helper_classes import *