from utils import *
from config import *
import time

class Node_Selector:
    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.village = self.config.setup_details['setup']['village']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        
        self.inp = self.config.setup_details['fbfs']['input_table']
        self.ori = self.config.setup_details['fbfs']['original_faces_table']
        self.nar = self.config.setup_details['fbfs']['narrow_faces_table']
        self.topo = self.village + self.config.setup_details['fbfs']['input_topo_suffix']

        self.corner_nodes = self.config.setup_details['fbfs']['corner_nodes']
        self.angle_thresh = self.config.setup_details['fbfs']['corner_nodes_angle_thresh']
        self.survey_no = self.config.setup_details['val']['survey_no_label']
        self.srid = self.config.setup_details['setup']['srid']
        
        self.covered_nodes = config.setup_details['fbfs']['covered_nodes_table']
        self.covered_edges = config.setup_details['fbfs']['covered_edges_table']
        self.covered_faces = config.setup_details['fbfs']['covered_faces_table']
        self.face_node_map = config.setup_details['fbfs']['face_node_map_table']
        
        self.temp_nodes_geom_table = config.setup_details['fbfs']['temp_nodes_geom_table']
        self.temp_translate_nodes = config.setup_details['fbfs']['temp_translate_nodes']
        self.temp_possible_snaps_table = config.setup_details['fbfs']['temp_possible_snaps']
        self.filtered_temp_possible_snaps_table = config.setup_details['fbfs']['filtered_temp_possible_snaps']
        
        self.snap_buffer_thresh = config.setup_details['fbfs']['snap_buffer_thresh']
        self.point_in_void_distance_thresh = config.setup_details['fbfs']['point_in_void_distance_thresh']
        self.possible_snaps_thresh_1 = int(config.setup_details['fbfs']['possible_snaps_thresh_1'])
        self.possible_snaps_thresh_2 = int(config.setup_details['fbfs']['possible_snaps_thresh_2'])

        self.vfn_table = config.setup_details['fp']['valid_farm_nodes_table']
        self.considered_snaps_debug = config.setup_details['fbfs']['considered_snaps_debug']
        
        self.eligible_area = config.setup_details['fbfs']['eligible_area_table']
        self.snappable_nodes = config.setup_details['fbfs']['snappable_nodes_table']
        self.unsnap_nodes = config.setup_details['fbfs']['unsnap_nodes_table']
        
        
        # NEEDS TO BE ADDED TO CONFIG
        self.farm_topo = self.village + "_farm_topo"
        
    def add_shifted_nodes(self, output_table):
        sql = f"""
            insert into {output_table}
            select
                node_id as node_id,
                1 as id,
                shifted_geom as geom
            from
                {self.village}.{self.temp_nodes_geom_table}
            where 
                shifted_geom is not null
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            
    def get_unshifted_node_ids(self): 
        sql = f"""
            with unshifted_nodes as (
                select
                    node_id,
                    original_geom as geom
                from
                    {self.village}.{self.temp_nodes_geom_table}
                where
                    shifted_geom is null
            ),
            degrees as (
                select
                    n.node_id as node_id,
                    n.geom as geom,
                    count(e.edge_id) as degree
                from
                    unshifted_nodes as n,
                    {self.topo}.edge_data as e
                where
                    (e.start_node = n.node_id or e.end_node = n.node_id)
                group by 
                    n.node_id, n.geom
            ),
            fp as (
                select
                    st_collect(geom) as geom
                from
                    {self.village}.{self.farmplots}
            )
            select
                d.node_id,
                not st_intersects(
                    st_buffer(d.geom,{self.point_in_void_distance_thresh},'join=mitre'), 
                    f.geom
                )
            from
                degrees as d,
                fp as f
            
            order by
                degree desc
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            node_ids = curr.fetchall()
        
        return node_ids
    
    def add_snap_option(self, output_table, node_id, eligible_area_check = False):
        if eligible_area_check:
            sql = f"""
                with covered_faces as (
                    select
                        st_collect(f.geom) as geom
                    from
                        {self.village}.{self.covered_faces} as f
                ),
                covered_edges as (
                    select
                        st_collect(e.geom) as geom
                    from
                        {self.village}.{self.covered_edges} as e
                ),
                eligible_area as (
                    select
                        st_union(geom) as geom
                    from 
                        {self.village}.{self.eligible_area} 
                ),
                cur_node as (
                    select 
                        node_id,
                        geom
                    from
                        {self.village}.{self.temp_translate_nodes}
                    where
                        node_id = {node_id}

                ),
                edges as (
                    select
                        st_union(geom) as geom
                    from
                        {self.farm_topo}.edge_data
                ),
                closest_point as (
                    select
                        node_id,
                        st_closestpoint(edges.geom, cur_node.geom) as geom
                    from
                        cur_node,
                        edges
                    where
                        st_distance(cur_node.geom,edges.geom) <= 12
                )
                insert into {output_table} (node_id, geom, type)
                select
                    n.node_id as node_id,
                    n.geom as geom,
                    'edge_snap' as type
                from
                    closest_point as n,
                    covered_faces as cf,
                    covered_edges as ce,
                    eligible_area as ea
                where
                    n.node_id = {node_id}
                    and
                    not coalesce(st_intersects(cf.geom, n.geom),false)
                    and
                    not coalesce(st_intersects(ce.geom, n.geom),false)
                    and 
                    coalesce(st_intersects(ea.geom, n.geom),true)
                returning
                    node_id
                ;
            """
        else:
            sql = f"""
                with covered_faces as (
                    select
                        st_collect(f.geom) as geom
                    from
                        {self.village}.{self.covered_faces} as f
                ),
                covered_edges as (
                    select
                        st_collect(e.geom) as geom
                    from
                        {self.village}.{self.covered_edges} as e
                ),
                cur_node as (
                    select 
                        node_id,
                        geom
                    from
                        {self.village}.{self.temp_translate_nodes}
                    where
                        node_id = {node_id}

                ),
                edges as (
                    select
                        st_union(geom) as geom
                    from
                        {self.farm_topo}.edge_data
                ),
                closest_point as (
                    select
                        node_id,
                        st_closestpoint(edges.geom, cur_node.geom) as geom
                    from
                        cur_node,
                        edges
                    where
                        st_distance(cur_node.geom,edges.geom) <= 12
                )
                insert into {output_table} (node_id, geom, type)
                select
                    n.node_id as node_id,
                    n.geom as geom,
                    'edge_snap' as type
                from
                    closest_point as n,
                    covered_faces as cf,
                    covered_edges as ce
                where
                    n.node_id = {node_id}
                    and
                    not coalesce(st_intersects(cf.geom, n.geom),false)
                    and
                    not coalesce(st_intersects(ce.geom, n.geom),false)
                returning
                    node_id
                ;
            """

        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            output = curr.fetchall()
            
        if output is None or len(output)==0:
            return False
        else:
            return True
    
    def add_translate_option(self, output_table, node_id, eligible_area_check = False):
        if eligible_area_check:
            sql = f"""
                with covered_faces as (
                    select
                        st_collect(f.geom) as geom
                    from
                        {self.village}.{self.covered_faces} as f
                ),
                covered_edges as (
                    select
                        st_collect(e.geom) as geom
                    from
                        {self.village}.{self.covered_edges} as e
                ),
                eligible_area as (
                    select
                        st_union(geom) as geom
                    from 
                        {self.village}.{self.eligible_area} 
                )
                insert into {output_table} (node_id, geom, type)
                select
                    n.node_id as node_id,
                    n.geom as geom,
                    'avg_translate' as type
                from
                    {self.village}.{self.temp_translate_nodes} as n,
                    covered_faces as cf,
                    covered_edges as ce,
                    eligible_area as ea
                where
                    n.node_id = {node_id}
                    and
                    not coalesce(st_intersects(cf.geom, n.geom),false)
                    and
                    not coalesce(st_intersects(ce.geom, n.geom),false)
                    and 
                    coalesce(st_intersects(ea.geom, n.geom),true)
                returning
                    node_id
                ;
            """
        else:
            sql = f"""
                with covered_faces as (
                    select
                        st_collect(f.geom) as geom
                    from
                        {self.village}.{self.covered_faces} as f
                ),
                covered_edges as (
                    select
                        st_collect(e.geom) as geom
                    from
                        {self.village}.{self.covered_edges} as e
                )
                insert into {output_table} (node_id, geom, type)
                select
                    n.node_id as node_id,
                    n.geom as geom,
                    'avg_translate' as type
                from
                    {self.village}.{self.temp_translate_nodes} as n,
                    covered_faces as cf,
                    covered_edges as ce
                where
                    n.node_id = {node_id}
                    and
                    not coalesce(st_intersects(cf.geom, n.geom),false)
                    and
                    not coalesce(st_intersects(ce.geom, n.geom),false)
                returning
                    node_id
                ;
            """
            
        
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            output = curr.fetchall()
            
        if output is None or len(output)==0:
            return False
        else:
            return True
        
    def add_farm_node_options(self, output_table, node_id, max_snaps, eligible_area_check = False):
        
        print(f"Adding farmnode option for node id {node_id} with max snaps :-", max_snaps)
        
        if eligible_area_check:
            sql = f"""
                with cur_node as (
                    select
                        node_id,
                        geom
                    from
                        {self.village}.{self.temp_translate_nodes}
                    where
                        node_id = {node_id}
                ),
                distanced_farm_nodes as (
                    select
                        n.node_id as node_id,
                        fn.node_id as farm_node_id,
                        fn.geom as geom,
                        st_distance(n.geom, fn.geom) as distance
                    from
                        cur_node as n
                    inner join
                        {self.village}.{self.vfn_table} as fn
                        on st_dwithin(n.geom,fn.geom,{self.snap_buffer_thresh})
                ),
                covered_faces as (
                    select
                        st_collect(f.geom) as geom
                    from
                        {self.village}.{self.covered_faces} as f
                ),
                covered_edges as (
                    select
                        st_collect(e.geom) as geom
                    from
                        {self.village}.{self.covered_edges} as e
                ),
                eligible_area as (
                    select
                        st_union(geom) as geom
                    from 
                        {self.village}.{self.eligible_area} 
                )
                insert into {output_table} (node_id, geom, farm_node_id, type)
                select
                    n.node_id as node_id,
                    n.geom as geom,
                    n.farm_node_id as farm_node_id,
                    'farm_node' as type
                from
                    distanced_farm_nodes as n,
                    covered_faces as cf,
                    covered_edges as ce,
                    eligible_area as ea
                where
                    not coalesce(st_intersects(cf.geom, n.geom),false)
                    and
                    not coalesce(st_intersects(ce.geom, n.geom),false)
                    and
                    coalesce(st_intersects(ea.geom, n.geom),true)
                order by
                    n.distance
                limit
                    {max_snaps}
                returning
                    node_id
                ;
            """
        else:
            sql = f"""
                with cur_node as (
                    select
                        node_id,
                        geom
                    from
                        {self.village}.{self.temp_translate_nodes}
                    where
                        node_id = {node_id}
                ),
                distanced_farm_nodes as (
                    select
                        n.node_id as node_id,
                        fn.node_id as farm_node_id,
                        fn.geom as geom,
                        st_distance(n.geom, fn.geom) as distance
                    from
                        cur_node as n
                    inner join
                        {self.village}.{self.vfn_table} as fn
                        on st_dwithin(n.geom,fn.geom,{self.snap_buffer_thresh})
                ),
                covered_faces as (
                    select
                        st_collect(f.geom) as geom
                    from
                        {self.village}.{self.covered_faces} as f
                ),
                covered_edges as (
                    select
                        st_collect(e.geom) as geom
                    from
                        {self.village}.{self.covered_edges} as e
                )
                insert into {output_table} (node_id, geom, farm_node_id, type)
                select
                    n.node_id as node_id,
                    n.geom as geom,
                    n.farm_node_id as farm_node_id,
                    'farm_node' as type
                from
                    distanced_farm_nodes as n,
                    covered_faces as cf,
                    covered_edges as ce
                where
                    not coalesce(st_intersects(cf.geom, n.geom),false)
                    and
                    not coalesce(st_intersects(ce.geom, n.geom),false)
                order by
                    n.distance
                limit
                    {max_snaps}
                returning
                    node_id
                ;
            """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            output = curr.fetchall()
        if output is None or len(output)==0:
            return 0
        else:
            return len(output)
            
        # add the possible snaps for the given node_id
    

    def prepare_original_edges(self, node_id):
        sql_query = f"""
            drop table if exists {self.village}.temp_original_edges;
            create table {self.village}.temp_original_edges as

            with endpoints as (
                select
                    node.node_id as node_id,
                    node.geom as node_geom,
                    (case
                        when edge.start_node = node.node_id then edge.end_node
                        else edge.start_node
                    end) as other_node_id,
                    edge.edge_id as edge_id,
                    edge.geom as edge_geom
                from
                    {self.topo}.node node,
                    {self.topo}.edge edge
                where
                    node.node_id = {node_id}
                    and
                    (
                        edge.start_node = {node_id}
                        or
                        edge.end_node = {node_id}
                    )
            )

            select
                endp.node_id as node_id,
                endp.other_node_id as other_node_id,
                endp.node_geom as node_geom,
                other_node.geom as other_geom,
                endp.edge_id as edge_id,
                endp.edge_geom as edge_geom
            from
                endpoints endp,
                {self.topo}.node other_node
            where
                endp.other_node_id = other_node.node_id
            ;
        """

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
    
    def prepare_farm_edges(self, input_table):
        sql_query = f"""
            drop table if exists {self.village}.temp_farm_edges;
            create table {self.village}.temp_farm_edges as

            with farm_nodes as (
                select
                    farm_node.node_id as farm_node_id,
                    inp.node_id as inp_node_id,
                    farm_node.geom as geom
                from
                    {self.farm_topo}.node farm_node,
                    {input_table} inp
                where
                    inp.farm_node_id = farm_node.node_id
            ),
            label_nodes as (
                select
                    farm_nodes.farm_node_id as farm_node_id,
                    farm_nodes.inp_node_id as inp_node_id,
                    farm_nodes.geom as node_geom,
                    edge.edge_id as edge_id,
                    (case
                        when edge.start_node = farm_nodes.farm_node_id then edge.end_node
                        else edge.start_node
                    end) as other_node_id,
                    edge.geom as edge_geom
                from
                    farm_nodes,
                    {self.farm_topo}.edge edge
                where
                    edge.start_node = farm_nodes.farm_node_id
                    or
                    edge.end_node = farm_nodes.farm_node_id
            )

            select
                label.inp_node_id as inp_node_id,
                label.farm_node_id as farm_node_id,
                label.other_node_id as other_node_id,
                label.node_geom as node_geom,
                other_node.geom as other_geom,
                label.edge_id as edge_id,
                label.edge_geom as edge_geom
            from
                label_nodes label,
                {self.farm_topo}.node other_node
            where
                label.other_node_id = other_node.node_id
            ;
        """

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
    
    def get_farm_node_scores(self):
        sql_query = f"""
            drop table if exists {self.village}.temp_farm_node_scores;
            create table {self.village}.temp_farm_node_scores as

            with farm_angles as (
                select
                    endp.farm_node_id as farm_node_id,
                    orig.edge_id as orig_edge_id,
                    degrees(st_angle(
                        endp.node_geom, endp.other_geom,
                        orig.node_geom, orig.other_geom
                    )) as angle,
                    endp.node_geom as farm_node_geom,
                    orig.edge_geom as orig_edge_geom
                from
                    {self.village}.temp_farm_edges endp,
                    {self.village}.temp_original_edges orig
            ),
            normal_angles as (
                select
                    farm_node_id,
                    orig_edge_id,
                    (case
                        when angle>180 then 360-angle
                        else angle
                    end) as angle,
                    farm_node_geom,
                    orig_edge_geom
                from
                    farm_angles
            ),
            min_angles as (
                select
                    farm_node_id,
                    orig_edge_id,
                    min(angle)/2 as min_angle,
                    farm_node_geom,
                    orig_edge_geom
                from
                    normal_angles
                group by
                    farm_node_id,
                    orig_edge_id,
                    farm_node_geom,
                    orig_edge_geom
            )

            select
                farm_node_id as node_id,
                exp(avg(log(cosd(min_angle)))) as gm_cos,
                farm_node_geom as geom
            from
                min_angles
            group by
                farm_node_id,
                farm_node_geom
            ;
        """

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
    
    def filter_options_angle(self, input_table, max_snaps, output_table, node_id):
        self.prepare_original_edges(node_id)
        self.prepare_farm_edges(input_table)
        self.get_farm_node_scores()
        
        sql_query = f"""
            with cur_node as (
                select
                    node_id,
                    geom
                from
                    {self.village}.{self.temp_translate_nodes}
                where
                    node_id = {node_id}
            ),
            scored_nodes as (
                select
                    inp.node_id as node_id,
                    inp.id as id,
                    inp.geom as geom,
                    inp.farm_node_id as farm_node_id,
                    coalesce(1 - farm_scores.gm_cos, 1) as angle_score,
                    st_distance(n.geom, inp.geom) as distance,
                    inp.type as type
                from
                    cur_node as n,
                    {input_table} as inp
                left join
                    {self.village}.temp_farm_node_scores farm_scores
                    on inp.farm_node_id = farm_scores.node_id
            ),
            final_score as (
                select
                    node_id,
                    id,
                    angle_score as penalty,
                    geom,
                    farm_node_id,
                    type
                from
                    scored_nodes
            )

            insert into {output_table} (node_id, id, farm_node_id, geom, penalty, type)
            select
                node_id,
                id,
                farm_node_id,
                geom,
                penalty,
                type
            from
                final_score
            where
                penalty < 0.03
            order by
                penalty
            limit
                {max_snaps}
            returning
                id
            ;
        """

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
            output = curs.fetchall()
        
        if output is None or len(output)==0:
            return 0
        else:
            print(output)
            return len(output)

    
    def filter_options(self, input_table, max_snaps, output_table, node_id):
        sql = f"""
            with cur_node as (
                select
                    node_id,
                    geom
                from
                    {self.village}.{self.temp_translate_nodes}
                where
                    node_id = {node_id}
            ),
            distanced_nodes as (
                select
                    inp.node_id as node_id,
                    inp.id as id,
                    inp.geom as geom,
                    inp.type as type,
                    st_distance(n.geom, inp.geom) as distance
                from
                    cur_node as n,
                    {input_table} as inp
            )
            insert into {output_table} (node_id, id, geom, type)
            select
                node_id,
                id,
                geom,
                type
            from
                distanced_nodes
            order by
                distance
            limit
                {max_snaps}
            returning
                id
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            output = curr.fetchall()

        if output is None or len(output)==0:
            return 0
        else:
            print(output)
            return len(output)
            
    def add_possible_snaps(self, input_table, output_table):
        sql = f"""
            insert into {output_table} (node_id, id, geom, type)
            select
                node_id,
                id,
                geom,
                type
            from
                {input_table}
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
    
    def create_possible_snaps_table(self, table, srid):
        sql = f"""
            drop table if exists {table};
            create table {table} (
                node_id integer,
                id integer,
                geom geometry(Point, {srid}),
                type varchar default null,
                primary key (node_id,id)  
            );
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            
    def add_possible_snaps_debug(self, nodes_table, debug_table):
        sql = f"""
            insert into {debug_table} (node_id, id, geom, penalty, type)
            select
                node_id,
                id,
                geom,
                penalty,
                type
            from
                {nodes_table}
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        
    def move_two_degree_nodes(self, two_degree_nodes_map):
        sql = f"""
            drop table if exists {two_degree_nodes_map};
            create table {two_degree_nodes_map} as 
            
            with unshifted_nodes as (
                select
                    node_id,
                    original_geom as geom
                from
                    {self.village}.{self.temp_nodes_geom_table}
                where
                    shifted_geom is null
            ),
            two_degree_nodes as (
                delete from {self.village}.{self.temp_nodes_geom_table} as a
                where 
                    a.node_id in (
                        select
                            node_id
                        from
                            unshifted_nodes
                    )
                    and 
                    a.node_id in (
                        select 
                            node_id
                        from
                            {self.village}.{self.unsnap_nodes}
                    )
                returning a.*
            )
            select
                *
            from
                two_degree_nodes
            ;
            
            alter table {two_degree_nodes_map}
            rename column original_geom to geom;
            
            alter table {two_degree_nodes_map}
            drop column shifted_geom;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        
    def add_parents(self, two_degree_nodes_map):
        
        add_column(self.psql_conn, two_degree_nodes_map, 'parent1', 'integer')
        add_column(self.psql_conn, two_degree_nodes_map, 'parent2', 'integer')
                
        sql = f"""
            update {two_degree_nodes_map} a
            set 
                parent1 = b.parent1,
                parent2 = b.parent2
            from 
                {self.village}.{self.unsnap_nodes} b
            where 
                a.node_id = b.node_id
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            
    def add_closest_points(self, two_degree_nodes_map):
        
        add_column(self.psql_conn, two_degree_nodes_map, 'closest_node_id', 'integer')
                
        sql = f"""
            with closest_nodes as (
                select
                    a.node_id as node_id,
                    (
                        select
                            b.node_id
                        from
                            {self.village}.{self.temp_nodes_geom_table} b
                        where
                            st_distance(a.geom, b.original_geom) <= 30
                        order by 
                            st_distance(a.geom, b.original_geom)
                        limit 1
                    ) as closest_node_id
                from
                    {two_degree_nodes_map} a
            )
            update {two_degree_nodes_map} a
            set 
                closest_node_id = b.closest_node_id
            from 
                closest_nodes b
            where 
                a.node_id = b.node_id
            ;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
    
    def add_unsnap_metadata(self, table, method="closest_node"):
        if method == "closest_node":
            self.add_closest_points(table)
        elif method == "parents":
            self.add_parents(table)
            
    def transformed_point(self,p1,p2,q1,q2,a):
        if (np.linalg.norm(p1 - p2) < 1e-10):
            return p1,p2,(p1+p2)/2
        v1 = np.array([p1[0]-q1[0],p1[1]-q1[1]])
        q2[0] += v1[0]
        q2[1] += v1[1]
        a[0] += v1[0]
        a[1] += v1[1]

        m1 = (p2[1]- p1[1])/(p2[0] - p1[0])
        m2 = (q2[1] - p1[1])/(q2[0] - p1[0])

        angle = math.atan(abs(m1-m2)/abs(1+m1*m2))
        rotation_matrix = 0
        if(m1 > m2):
            rotation_matrix = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
        else:
            rotation_matrix = np.array([[np.cos(angle), np.sin(angle)], [-np.sin(angle), np.cos(angle)]])
            
        a_r = p1 + np.dot(rotation_matrix,a - p1)
        scale = np.linalg.norm(p2 - p1) / np.linalg.norm(q2 - p1)
        a_r = p1 + scale*(a_r - p1)
        # print(a, a_r)
        return p1,p2,a_r
            
    def add_unregistered_nodes(self, face_id, two_degree_nodes_map, updated_nodes_table, method = "closest_node"):
        if method == "closest_node":
            sql = f"""
                with translation as (
                    select
                        old.node_id as node_id,
                        st_x(new.geom) - st_x(old.original_geom) as x,
                        st_y(new.geom) - st_y(old.original_geom) as y
                    from
                        {self.village}.{self.temp_nodes_geom_table} as old
                    join
                        {updated_nodes_table} as new
                        on old.node_id = new.node_id
                )
                insert into {updated_nodes_table} (node_id, geom)
                select
                    a.node_id as node_id,
                    st_translate(
                        a.geom,
                        b.x,
                        b.y
                    ) as geom
                from
                    {two_degree_nodes_map} as a
                left join
                    translation as b
                    on a.closest_node_id = b.node_id
                where
                    b.node_id is not null
                ;
            """
            with self.psql_conn.connection().cursor() as curr:
                curr.execute(sql)
                
        elif method=="parents":
            sql = f"""
                with translation as (
                    select
                        old.node_id as node_id,
                        new.geom as new_geom,
                        old.original_geom as old_geom
                    from
                        {self.village}.{self.temp_nodes_geom_table} as old
                    join
                        {updated_nodes_table} as new
                        on old.node_id = new.node_id
                )
                select 
                    a.node_id,
                    st_x(g.geom),
                    st_y(g.geom),
                    a.parent1,
                    st_x(p1.old_geom),
                    st_y(p1.old_geom),
                    st_x(p1.new_geom),
                    st_y(p1.new_geom),
                    a.parent2,
                    st_x(p2.old_geom),
                    st_y(p2.old_geom),
                    st_x(p2.new_geom),
                    st_y(p2.new_geom)
                from
                    {two_degree_nodes_map} as a
                left join
                    {self.topo}.node as g
                    on a.node_id = g.node_id
                left join
                    translation as p1
                    on a.parent1 = p1.node_id
                left join
                    translation as p2
                    on a.parent2 = p2.node_id
            """
            with self.psql_conn.connection().cursor() as curr:
                curr.execute(sql)
                set_c_points = curr.fetchall()
            
            # print(set_c_points)
            # input("wait3")
            
            final_points = []
                
            for point in set_c_points:
                a = np.array(point[1:3])
                q1 = np.array(point[4:6])
                q2 = np.array(point[9:11])
                p1 = np.array(point[6:8])
                p2 = np.array(point[11:13])
                _,_,a_r = self.transformed_point(p1,p2,q1,q2,a)
                final_points.append((point[0],a_r[0],a_r[1]))
            
            # print(final_points)
            # input("wait4")
            
            sql = f"""
                insert into {updated_nodes_table} (node_id, geom)
                values (%s, st_point(%s,%s,{self.srid}));
            """
            with self.psql_conn.connection().cursor() as curr:
                curr.executemany(sql,final_points)
            
    
    def get_possible_snaps(self, face_id, possible_snaps_table, layered=False, two_degree_nodes_map=None, only_translate=False, eligible_area=False):
        st = time.time()
        get_nodes_geom(self.psql_conn, self.village, self.topo, self.temp_nodes_geom_table,
                                    self.face_node_map, self.covered_nodes, face_id)        
        average_translate_face_nodes(self.psql_conn, self.village, self.topo, face_id, self.face_node_map,
                                     self.covered_nodes, self.temp_translate_nodes, self.temp_nodes_geom_table)
        et = time.time()
        print("Duration of generating average translate face :-",et-st)
        
        if eligible_area:
            if not check_table_exists(self.psql_conn, self.village, self.eligible_area):
                print("Eligible area table does not exist")
                return None
                
        self.create_possible_snaps_table(possible_snaps_table, self.srid)
        
        self.add_shifted_nodes(possible_snaps_table)
        
        if layered:
            self.move_two_degree_nodes(two_degree_nodes_map)
            
        if not check_column_exists(self.psql_conn, self.village, self.considered_snaps_debug, 'penalty'):
            add_column(self.psql_conn, self.village+'.'+self.considered_snaps_debug, 'penalty', 'double precision')
        
        node_ids = self.get_unshifted_node_ids()
        
        for node_id, node_in_void in node_ids:
            print("\nCovering Node id :-",node_id,"\nNode in void ?", node_in_void)
            sql = f"""
                drop table if exists {self.village}.{self.temp_possible_snaps_table};
                create table {self.village}.{self.temp_possible_snaps_table} (
                    node_id integer,
                    id serial,
                    farm_node_id integer default null,
                    geom geometry(Point, {self.srid}),
                    penalty double precision default null,
                    type varchar default null
                );
                drop table if exists {self.village}.{self.filtered_temp_possible_snaps_table};
                create table {self.village}.{self.filtered_temp_possible_snaps_table} (
                    node_id integer,
                    id serial,
                    farm_node_id integer default null,
                    geom geometry(Point, {self.srid}),
                    penalty double precision default null,
                    type varchar default null
                );
            """
            with self.psql_conn.connection().cursor() as curr:
                curr.execute(sql)
                
            count = 0
            if node_in_void:
                st = time.time()
                if self.add_translate_option(self.village+'.'+self.temp_possible_snaps_table, node_id, eligible_area):
                    count += 1
                et = time.time()
                print("Duration of adding translate option :-",et-st)
            
            st = time.time()
            num_added = 0
            if not only_translate:
                num_added = self.add_farm_node_options(self.village+'.'+self.temp_possible_snaps_table,node_id, self.possible_snaps_thresh_1-count, eligible_area)
            et = time.time()
            print("Duration of adding farm node option :-",et-st)
            
            count += num_added
            
            st = time.time()
            number_selected = 0
            if not only_translate:
                number_selected = self.filter_options_angle(self.village+'.'+self.temp_possible_snaps_table, self.possible_snaps_thresh_2, 
                                                    self.village+'.'+self.filtered_temp_possible_snaps_table, node_id)
            et = time.time()
            print("Duration of filtering snaps :-",et-st)
            
            print("Number of nodes considered :-",count)
            print("Number of nodes selected", number_selected)
            if number_selected==0:
                st = time.time()
                translate_option_added = self.add_translate_option(self.village+'.'+self.filtered_temp_possible_snaps_table, node_id, eligible_area)
                edge_snap_option_added = self.add_snap_option(self.village+'.'+self.filtered_temp_possible_snaps_table, node_id, eligible_area)
                if not (translate_option_added or edge_snap_option_added):
                    return None
                et = time.time()
                print("Duration of adding translate option :-",et-st)
                
            print(f"Snaps for nodes stored in '{self.village}.{self.filtered_temp_possible_snaps_table}'")
            if config.DEBUG_MODE:
                a = input("\nPress Enter to continue : ")
            
            self.add_possible_snaps(self.village+'.'+self.filtered_temp_possible_snaps_table, possible_snaps_table)
            # self.add_possible_snaps_debug(self.village+'.'+self.temp_possible_snaps_table,
            #                               self.village+'.'+self.considered_snaps_debug)
            self.add_possible_snaps_debug(self.village+'.'+self.filtered_temp_possible_snaps_table,
                                          self.village+'.'+self.considered_snaps_debug)
            
        sql = f"""
            select 
                node_id,
                array_agg(id)
            from
                {possible_snaps_table}
            group by
                node_id
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            snap_ids = curr.fetchall()
            
        print(f"\nFound Possible snaps for face_id {face_id} as", snap_ids)
        if config.DEBUG_MODE:
            a = input("\nPress Enter to continue : ")
            
        
        return snap_ids
                