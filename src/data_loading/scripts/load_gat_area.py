import os
import subprocess
import argparse
import pandas as pd
from utils import *
from config import *

def convert_row(row):
    try:
        row[0] = int(row[0])
        row[1] = str(row[1])
        row[2] = float(row[2])
        return row
    except (ValueError, TypeError):
        return None


def load_gat_area_from_excel(psql_conn, path, village, table_name, gat_no, area_col_name):
    df = pd.read_excel (path, usecols='A, B, C', dtype=str)
    df = df.apply(convert_row, axis=1).dropna()
    conn = psql_conn.connection()
    new_df = df.dropna()
    gat_area_values = new_df.values.tolist()
    sql = f"""
        drop table if exists {village}.{table_name};
        create table {village}.{table_name} (
            gid serial, 
            {gat_no} varchar(20), 
            {area_col_name} float
        );
    """
    with conn.cursor() as curr:
        curr.execute(sql)
        curr.executemany(f"INSERT INTO {village}.{table_name} VALUES(%s,%s,%s)", gat_area_values)
        
    # not needed
    sql = f'''
    UPDATE {village}.{table_name} as p
        SET {area_col_name} = b.{area_col_name}
        FROM (
            SELECT {gat_no}, sum({area_col_name}) AS {area_col_name}
            FROM {village}.{table_name}
            where {area_col_name}>0
            GROUP BY {gat_no}
        ) AS b
        WHERE b.{gat_no} = p.{gat_no}
    '''
    with conn.cursor() as curr:
        curr.execute(sql)
    sql = f'''
    DELETE FROM {village}.{table_name}
    WHERE gid IN
        (SELECT gid
        FROM 
            (SELECT gid,
            ROW_NUMBER() OVER( PARTITION BY {gat_no}
            ORDER BY  gid ) AS row_num
            FROM {village}.{table_name} ) t
            WHERE t.row_num > 1 );
    '''
    with conn.cursor() as curr:
        curr.execute(sql)
    

def load_gat_area(config, psql_conn, path_to_gat_area):
    gat_no_label =  config.setup_details['val']['gat_no_label']
    for root, dirs, files in os.walk(path_to_gat_area, topdown=True):
        for file in files:
            file_location = os.path.join(root, file)
            table_name = config.setup_details['data']['gat_area_table']
            gat_area_col_name = config.setup_details['gat_jitter']['gat_area_column_name']
            # gat_original =  config.setup_details['data']['gat_map_table']
            village_name = config.setup_details['setup']['village']
            if file.endswith(".xlsx"):
                load_gat_area_from_excel(psql_conn, file_location,village_name,table_name, gat_no_label, gat_area_col_name)

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-p", "--path", help="Path to data",
                        required=True, default="")
    parser.add_argument("-v", "--village", help="Village name",
                        required=True, default="")
    
    argument = parser.parse_args()
    path_to_data = argument.path
    village = argument.village
    
    if path_to_data=="" or village=="":
        print("ERROR")
        exit()
        
    config = Config()
    pgconn = PGConn(config)
    
    config.setup_details['setup']['village'] = village
    
    load_gat_area(config,pgconn,path_to_data)