from config import *
from utils import *
from scripts import *
import argparse
import os
import subprocess

def dataloading(path_to_data = "", toggle = ""):
    config = Config()
    
    if path_to_data != "":
        config.setup_details["data"]["path"] = path_to_data
    if toggle != "":
        config.setup_details["data"]["toggle"] = toggle
    
    pgconn = PGConn(config)
    
    return DataLoading(config,pgconn)

class DataLoading:
    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.path = self.config.setup_details["data"]["path"]
        self.toggle = self.config.setup_details["data"]["toggle"]
        self.toggle = int(self.toggle)
        
    def run(self):
        if self.path == "":
            print("Data path not set for data")
            return
        if self.toggle == "":
            print("Toggle not given")
            return
        self.toggle = int(self.toggle)
        for (root,dirs,files) in os.walk(self.path, topdown=True):
            
            if root[len(self.path):].count(os.sep)!=self.toggle:
                continue
                
            vil_name_start_index = 1
            vincode = os.path.basename(root).split('_')[0]
            
            if vincode.isdigit() == False:
                vil_name_start_index = 0
            
            village = ("_".join((os.path.basename(root).split('_'))[vil_name_start_index:])).lower()
            village = ''.join(e for e in village if e.isalnum())
            # vincode = os.path.basename(root).split('_')[0]
            print("Processing village",village)
            
            self.config.setup_details['setup']['village'] = village
            
            create_schema(self.psql_conn, village, delete_original= False)
                
            for dir in dirs:
                if dir.startswith("09"):
                    try:
                        load_survey_plots(self.config,self.psql_conn, os.path.join(root,dir))
                    except:
                        print("Error in loading survey plots for village",village)
                    
                elif dir.startswith("13"):
                    try:
                        load_gat_plots(self.config,self.psql_conn, os.path.join(root,dir))
                    except:
                        print("Error in loading gat plots for village",village)
                    
                elif dir.startswith("14"):
                    try:
                        load_gcps(self.config,self.psql_conn, os.path.join(root,dir))
                    except:
                        print("Error in loading gcps for village",village)
                    
                elif dir.startswith("15"):
                    try:
                        load_akarbandh(self.config,self.psql_conn, os.path.join(root,dir))
                    except:
                        print("Error in loading akarbandh for village",village)
                    
                elif dir.startswith("16"):
                    try:
                        load_cadastrals(self.config,self.psql_conn, os.path.join(root,dir))
                    except:
                        print("Error in loading cadastrals for village",village)
                    
                elif dir.startswith("17"):
                    try:
                        load_gat_area(self.config,self.psql_conn, os.path.join(root,dir))
                    except:
                        print("Error in loading gat area for village",village)

def farmplotloading(path_to_farmplots = "", schema = "" , option = ""):
    config = Config()
    
    if path_to_farmplots != "":
        config.setup_details["data"]["farmplots_path"] = path_to_farmplots
    if option != "":
        config.setup_details['data']['farmplot_load_option'] = option
    
    if schema != "":
        config.setup_details['data']['farmplot_combined_schema'] = schema
    pgconn = PGConn(config)
    
    return FarmplotLoading(config,pgconn)

class FarmplotLoading:
    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.path = self.config.setup_details["data"]["farmplots_path"]
        self.toggle = self.config.setup_details["data"]["toggle"]
        self.toggle = int(self.toggle)
        self.schema = self.config.setup_details['data']['farmplot_combined_schema']
        self.load_option = self.config.setup_details['data']['farmplot_load_option']
        self.temp = "temp_farmplots_tile_for_all" if schema != "" else "temp_farmplots_tile"
           
    def run(self):
        if self.path == "":
            print("Data path not set for farmplots")
            return
        srid = self.config.setup_details['setup']['srid']
        table_name = self.config.setup_details["data"]["original_farmplots_table"]
        for (root,dirs,files) in os.walk(self.path, topdown=True):
            
            try:
                village = os.path.basename(root).split('_')[0].lower()
                # village = village.replace(' ','')
                if self.load_option == "1":
                    village = self.schema
                village = ''.join(e for e in village if e.isalnum())
                
                geom_files = []
                
                for file in files:
                    if file.endswith(".shp") or file.endswith(".kml"):
                        # village = file.split('_')[0].lower()
                        # village = village.replace(' ','')
                        print(file,village)
                        file_location = os.path.join(root,file)
                        geom_files.append(file_location)
                        
                if len(geom_files) == 0:
                    continue
                
                create_schema(self.psql_conn, village, delete_original= False)
                drop_table(self.psql_conn, village, table_name)
                
                for location in geom_files:
                    ogr2ogr_cmd = [
                        'ogr2ogr','-f','PostgreSQL','-t_srs',f'EPSG:{srid}',
                        'PG:dbname=' + self.psql_conn.details["database"] + ' host=' +
                            self.psql_conn.details["host"] + ' user=' + self.psql_conn.details["user"] +
                            ' password=' + self.psql_conn.details["password"],
                        location,
                        '-lco', 'OVERWRITE=YES',
                        '-lco', 'GEOMETRY_NAME=geom',
                        '-lco', 'schema=' + village, 
                        '-lco', 'SPATIAL_INDEX=GIST',
                        '-nlt', 'PROMOTE_TO_MULTI',
                        '-nln', self.temp
                    ]
                    subprocess.run(ogr2ogr_cmd) 
                    
                    sql = f"""
                        create table if not exists {village}.{table_name} 
                        as table {village}.{self.temp};
                        
                        insert into {village}.{table_name} 
                        select * from {village}.{self.temp};
                    """
                    with self.psql_conn.connection().cursor() as curr:
                        curr.execute(sql)
                        
                
                add_column(self.psql_conn, village+'.'+table_name, "gid", "serial")
                if self.schema != "":
                    with self.psql_conn.connection().cursor() as curr:
                        curr.execute(f"CREATE INDEX gist_index ON {village}.{table_name} USING GIST(geom);")
                
            except Exception as e:
                print("Error in loading farmplots for village",village)
                print(e)
        
if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-p", "--path", help="Path to data",
                        required=False, default="")
    parser.add_argument("-t", "--toggle", help="0 for village path, 1 for taluka path, 2 for district path and 3 for state path",
                        required=False, default="")
    parser.add_argument("-f", "--farmpath", help="Path to farmplots",
                        required=False, default="")
    parser.add_argument("-s", "--schema", help="schema name where farmplot table will be added (used when combined farmplots is provided)",
                        required=False, default="")
    parser.add_argument("-o", "--farmplot_toggle", help="0 for village wise farmplots, 1 for combined schema",
                        required=False, default="")
    
    argument = parser.parse_args()
    path_to_data = argument.path
    toggle = argument.toggle
    path_to_farmplots = argument.farmpath
    schema = argument.schema
    option = argument.farmplot_toggle
    dl = dataloading(path_to_data, toggle)
    dl.run()
    
    fl = farmplotloading(path_to_farmplots, schema , option)
    fl.run()