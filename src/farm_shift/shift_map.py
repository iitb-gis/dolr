from utils import *
from config import *
import argparse

def shift_map(village , toggle):
    config = Config()
    if village != "":
        config.setup_details['setup']['village'] = village
    pgconn = PGConn(config)
    
    return Map_Shift(config,pgconn , toggle)

class Map_Shift:

    def __init__(self, config, psql_conn , toggle):
        self.config = config
        self.psql_conn = psql_conn
        self.village = self.config.setup_details['setup']['village']
        self.gcp = self.config.setup_details['data']['gcp_table']
        if toggle == '0':
            self.map = self.config.setup_details['data']['shifted_faces_table']
        elif toggle == '1':
            self.map = self.config.setup_details['local_jitter']['jitter_spline_output']
        elif toggle == '2':
            self.map = self.config.setup_details['data']['survey_georeferenced_table']
        self.map_trijunctions = self.map + self.config.setup_details['shift']['trijunctions_suffix']
        self.shift_gcp_map = self.map + self.config.setup_details['shift']['gcp_map_suffix']
        self.translated_map_nodes = self.map+self.config.setup_details['shift']['translated_map_nodes']
        self.map_after_translation = self.map+ self.config.setup_details['shift']['map_after_translation']
        self.map_topo = self.village +"_"+ self.map + self.config.setup_details['shift']['map_topo_suffix']


    def shift(self):
        create_topo(self.psql_conn, self.village, self.map_topo, self.map)
        get_corner_nodes(self.psql_conn, self.map_topo, self.village, self.map_trijunctions, only_trijunctions=True)
        create_node_labels(self.psql_conn, self.village, self.map, self.map_trijunctions, village_boundary_label='vb')
        create_distance_gcp_map(self.psql_conn, self.village, self.map_trijunctions, self.gcp, self.shift_gcp_map , distance_thresh=10 , jitter = True)
        sql = f"""
            with nodes as (
                select
                    node_id as node_id,
                    gcp_geom as shifted_geom,
                    node_geom as original_geom
                from
                    {self.village}.{self.shift_gcp_map}
            ),
            averages as(
                select
                    coalesce(avg(st_x(shifted_geom)-st_x(original_geom)),0) as delta_x,
                    coalesce(avg(st_y(shifted_geom)-st_y(original_geom)),0) as delta_y
                from
                    nodes as n
                )
            select * from averages;
            
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            results = curr.fetchall()
        delta_x = results[0][0]
        delta_y = results[0][1]
        sql = f'''
            drop table if exists {self.village}.{self.translated_map_nodes};
            create table {self.village}.{self.translated_map_nodes} as
            select
                n.node_id as node_id,
                st_translate(n.geom, {delta_x}, {delta_y}) as geom 
            from
                {self.map_topo}.node as n
            ;
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        print(delta_x , delta_y)
        copy_table(self.psql_conn, self.village+"."+self.map, self.village+"."+self.map_after_translation)
        #update_topology(self.psql_conn, self.map_topo, self.village+"."+self.translated_facefit_nodes)
        update_translation(self.psql_conn, self.village, self.map, self.map_after_translation,  delta_x, delta_y )
        
    def run(self):
        self.shift()

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    parser.add_argument("-t", "--toggle", help="0 for facefit map, 1 for jitter spline, 2 for survey georeferenced",
                        required=False, default="")
    argument = parser.parse_args()
    village = argument.village
    toggle = argument.toggle
    farm_shift = shift_map(village , toggle)
    farm_shift.run()
