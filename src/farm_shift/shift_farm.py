from utils import *
from config import *
import argparse

def shift_farm(village):
    config = Config()
    if village != "":
        config.setup_details['setup']['village'] = village
    pgconn = PGConn(config)
    
    return Farmplot_shift(config,pgconn)

class Farmplot_shift:

    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.village = self.config.setup_details['setup']['village']
        self.farm_topo =  self.village+ self.config.setup_details['fp']['farm_topo_suffix']
        self.gcp = self.config.setup_details['data']['gcp_table']
        self.farm_trijunctions = self.config.setup_details['fp']['farm_trijunctions']
        self.farm_gcp_map = self.config.setup_details['fp']['farm_gcp_map']
        self.farm_topo_shifted = self.village + self.config.setup_details['fp']['shifted_topo_suffix']
        self.translated_farm_nodes = self.config.setup_details['fp']['translated_farm_nodes']
        self.farmplots_after_translation = self.config.setup_details['fp']['farmplots_after_translation']
        self.farmplots = self.config.setup_details['data']['farmplots_table']


    def shift(self):
        get_corner_nodes(self.psql_conn, self.farm_topo, self.village, self.farm_trijunctions, only_trijunctions=True)
        create_distance_gcp_map(self.psql_conn, self.village, self.farm_trijunctions, self.gcp, self.farm_gcp_map , distance_thresh=10, jitter = True , bounds = ((-0.02, 0.02), (1.0, 1.0), (1.0, 1.0), (-10, 10), (-10, 10)))
        sql = f"""
            with nodes as (
                select
                    node_id as node_id,
                    gcp_geom as shifted_geom,
                    node_geom as original_geom
                from
                    {self.village}.{self.farm_gcp_map}
            ),
            averages as(
                select
                    coalesce(avg(st_x(shifted_geom)-st_x(original_geom)),0) as delta_x,
                    coalesce(avg(st_y(shifted_geom)-st_y(original_geom)),0) as delta_y
                from
                    nodes as n
                )
            select * from averages;
            
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            results = curr.fetchall()
        delta_x = results[0][0]
        delta_y = results[0][1]
        sql = f'''
            drop table if exists {self.village}.{self.translated_farm_nodes};
            create table {self.village}.{self.translated_farm_nodes} as
            select
                n.node_id as node_id,
                st_translate(n.geom, {delta_x}, {delta_y}) as geom 
            from
                {self.farm_topo}.node as n
            ;
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        print(delta_x , delta_y)
        #update_topology(self.psql_conn, self.farm_topo, self.village+"."+self.translated_farm_nodes)
        copy_table(self.psql_conn, self.village+"."+self.farmplots, self.village+"."+self.farmplots_after_translation)
        update_translation(self.psql_conn, self.village, self.farmplots, self.farmplots_after_translation,  delta_x, delta_y)
        
    def run(self):
        self.shift()

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    argument = parser.parse_args()
    village = argument.village
    farm_shift = shift_farm(village)
    farm_shift.run()
