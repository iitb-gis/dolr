from config import *
from utils import *
import argparse


def t_junction_fixer(village = ""):
    config = Config()

    pgconn = PGConn(config)
    if village != "":    
        config.setup_details['setup']['village'] = village
    
    return Tri_Junction_Creator(config,pgconn)


    
class Tri_Junction_Creator:
    def __init__(self, config: Config, psql_conn: PGConn):
        self.config = config
        self.psql_conn = psql_conn
        self.village = self.config.setup_details['setup']['village']
        self.srid = self.config.setup_details['setup']['srid']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        self.trijunctions = self.config.setup_details['fp']['trijunction_table']
        self.topo = self.config.setup_details['fp']['farm_topo_suffix']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        self.trij = self.config.setup_details['fp']['trij']
        self.tjunc = self.config.setup_details['fp']['tjunc']
        self.fp_midlines_edges = self.config.setup_details['fp']['fp_midlines_edges_table']
        self.edge_thick = self.config.setup_details['fp']['edge_thick']
        self.edges_corrected = self.config.setup_details['fp']['edges_corrected']
        self.all_node_schema = self.village+config.setup_details['fp']['farm_topo_suffix']
        self.updated_topo = self.village+config.setup_details['fp']['updated_topo']
        self.tol = self.config.setup_details['fp']['topo_tol']
        self.fp_mid_poly = self.config.setup_details['fp']['fp_midline_polygons']
        self.fp_mid_poly_filtered = self.config.setup_details['fp']['fp_midline_polygons_filtered']
        self.area_thresh = self.config.setup_details['fp']['valid_fp_area_threshold']
        self.intersection_thresh = self.config.setup_details['fp']['filtering_intersection_threshold']
        self.seg_tol = self.config.setup_details['fp']['seg_tol']
        self.seg_length = self.config.setup_details['fp']['seg_length']

        if self.village == "":
            print("ERROR")
            exit()
            
    def store_trijunction(self):
        edges = f"{self.all_node_schema}.edge"  
        nodes = f"{self.all_node_schema}.node"          
        trijunction_table = self.village+"."+self.trijunctions
        trij = self.village+"."+self.trij
        tjunc = self.village+"."+self.tjunc
        topo = self.all_node_schema
        
        sql_query = f"""
                   
                   Drop table if exists {trijunction_table};
                   Create table {trijunction_table} as
                    select 
                        n.node_id, n.geom, e.edge_id, e.start_node, e.end_node
                    from 
                        {nodes} n
                    join 
                        {edges} e ON n.node_id = e.start_node OR n.node_id = e.end_node
                    group by
                         n.node_id, e.edge_id,  e.start_node, e.end_node;
                    
                    delete from {trijunction_table}
                    where node_id in (
                    select
                         node_id
                    from(
                        select
                          node_id, count(*) AS count_per_node_id
                        from
                            {trijunction_table}
                        group by
                             node_id
                    ) AS NodeID_counts
                    WHERE count_per_node_id!= 3
                );
                   
                alter table {trijunction_table}
                add column buffered_geom geometry;
                update {trijunction_table}
                SET buffered_geom = ST_Boundary(ST_Buffer(geom, 5.2));
        
                
                

                
                drop table if exists {trij};
                create table {trij} AS
                select distinct
                    tj.node_id,
                    tj.geom AS node_geom,
                    e.edge_id,
                    e.geom AS edge_geom,
                    degrees(ST_Azimuth(ST_StartPoint(e.geom), ST_EndPoint(e.geom))) AS edge_angle
                FROM 
                    {trijunction_table} tj,
                    {edges} e
                WHERE
                    st_intersects(tj.buffered_geom, e.geom)
                ;

                
                alter table {trij}
                add column node_outside geometry(Point);
                update {trij}
                set node_outside = 
                    case
                        when ST_Distance(node_geom, ST_StartPoint(edge_geom)) > 5 then ST_StartPoint(edge_geom)
                        when ST_Distance(node_geom, ST_EndPoint(edge_geom)) > 5 then ST_EndPoint(edge_geom)
                        else null
                    end;

                delete from {trij}
                    where node_id in (
                    select
                         node_id
                    from(
                        select
                          node_id, count(*) AS count_per_node_id
                        from
                            {trij}
                        group by
                             node_id
                    ) AS NodeID_counts
                    where count_per_node_id > 3
                );

                

                alter table {trij}
                add column group_rank integer;
                Update {trij}
                SET group_rank = subquery.group_rank1
                From(
                    SELECT node_id, edge_id, ROW_NUMBER() OVER(PARTITION BY node_id, node_geom ORDER BY edge_id) AS group_rank1
                        FROM {trij}
                    ) AS subquery
                WHERE {trij}.node_id = subquery.node_id AND {trij}.edge_id = subquery.edge_id;
                



                drop table if exists temp_query;
                create temporary table temp_query AS
                select 
                    node_id,
                    node_geom,
                    ROW_NUMBER() OVER(PARTITION BY node_id, node_geom ORDER BY node_id) AS group_rank,
                    max(case when group_rank = 1 then edge_id else null end) AS edge_id_1,
                    max(case when group_rank = 1 then edge_geom else null end)::geometry AS edge_geom_1,
                    max(case when group_rank = 1 then edge_angle else null end) AS edge_angle_1,
                    max(case when group_rank = 1 then node_outside else null end)::geometry AS node_outside_1,
                    max(case when group_rank = 2 then edge_id else null end) AS edge_id_2,
                    max(case when group_rank = 2 then edge_geom else null end)::geometry AS edge_geom_2,
                    max(case when group_rank = 2 then edge_angle else null end) AS edge_angle_2,
                    max(case when group_rank = 2 then node_outside else null end)::geometry AS node_outside_2,
                    max(case when group_rank = 3 then edge_id else null end) AS edge_id_3,
                    max(case when group_rank = 3 then edge_geom else null end)::geometry AS edge_geom_3,
                    max(case when group_rank = 3 then edge_angle else null end) AS edge_angle_3,
                    max(case when group_rank = 3 then node_outside else null end)::geometry AS node_outside_3
                FROM {trij}
                group by node_id, node_geom;
                drop table {trij};
                create table {trij} AS
                select * from temp_query;

                drop table if exists {tjunc};

                create table {tjunc} as
                select 
                    *
                from
                    {trij}
                where 
                
                    (
                        (least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) between 165 and 195) 
                        and least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) between 55 and 150 
                        and least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) between 55 and 150
                    )
                    or
                    (
                        (least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) between 165 and 195) 
                        and least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) between 55 and 150 
                        and least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) between 55 and 150
                    )
                    or
                    (
                        (least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) between 165 and 195) 
                        and least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) between 55 and 150 
                        and least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) between 55 and 150
                    )
                    or 
                    (
                        (least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) between 0 and 12) 
                        and least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) between 55 and 150 
                        and least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) between 55 and 150
                    )
                    or 
                    (
                        (least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) between 0 and 12) 
                        and least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) between 55 and 150
                        and least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) between 55 and 150
                    )
                    or 
                    (
                        (least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) between 0 and 12) 
                        and least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) between 55 and 150 
                        and least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) between 55 and 150
                    );
                            

                    
                alter table {tjunc}
                add column extended_edge_1 geometry,
                add column extended_edge_2 geometry,
                add column extended_edge_3 geometry;

                update {tjunc}
                SET 
                    extended_edge_1 =  ST_MakeLine(
                                                ST_Translate(ST_StartPoint(edge_geom_1), 
                                                            -25 * sin(ST_Azimuth(ST_StartPoint(edge_geom_1), ST_EndPoint(edge_geom_1))), 
                                                            -25 * cos(ST_Azimuth(ST_StartPoint(edge_geom_1), ST_EndPoint(edge_geom_1)))),
                                                
                                                ST_Translate(ST_ENdPoint(edge_geom_1), 
                                                            25 * sin(ST_Azimuth(ST_StartPoint(edge_geom_1), ST_EndPoint(edge_geom_1))), 
                                                            25 * cos(ST_Azimuth(ST_StartPoint(edge_geom_1), ST_EndPoint(edge_geom_1))))
                                               
                                            ),
                    
                    extended_edge_2 =  ST_MakeLine(
                                                ST_Translate(ST_StartPoint(edge_geom_2), 
                                                            -25 * sin(ST_Azimuth(ST_StartPoint(edge_geom_2), ST_EndPoint(edge_geom_2))), 
                                                            -25 * cos(ST_Azimuth(ST_StartPoint(edge_geom_2), ST_EndPoint(edge_geom_2)))),
                                                
                                                ST_Translate(ST_EndPoint(edge_geom_2), 
                                                            25 * sin(ST_Azimuth(ST_StartPoint(edge_geom_2), ST_EndPoint(edge_geom_2))), 
                                                            25 * cos(ST_Azimuth(ST_StartPoint(edge_geom_2), ST_EndPoint(edge_geom_2))))
                                               
                                            ),

                    extended_edge_3 =  ST_MakeLine(
                                                ST_Translate(ST_StartPoint(edge_geom_3), 
                                                            -25 * sin(ST_Azimuth(ST_StartPoint(edge_geom_3), ST_EndPoint(edge_geom_3))), 
                                                            -25 * cos(ST_Azimuth(ST_StartPoint(edge_geom_3), ST_EndPoint(edge_geom_3)))),
                                                
                                                ST_Translate(ST_ENdPoint(edge_geom_3), 
                                                            25 * sin(ST_Azimuth(ST_StartPoint(edge_geom_3), ST_EndPoint(edge_geom_3))), 
                                                            25 * cos(ST_Azimuth(ST_StartPoint(edge_geom_3), ST_EndPoint(edge_geom_3))))
                                               
                                            );
                    
                    alter table {tjunc}
                    add column node_corrected geometry ;
                    update {tjunc}
                    set node_corrected = 
                    case 
                        when least(
                                abs(least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) - 90),
                                abs(least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) - 270)
                            ) < least(
                                abs(least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) - 90),
                                abs(least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) - 270)
                            ) and least(
                                abs(least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) - 90),
                                abs(least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) - 270)
                            ) < least(
                                abs(least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) - 90),
                                abs(least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) - 270)
                            )
                        then st_intersection(extended_edge_1, extended_edge_2)
                        
                        when least(
                                abs(least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) - 90),
                                abs(least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) - 270)
                            ) < least(
                                abs(least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) - 90),
                                abs(least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) - 270)
                            ) and least(
                                abs(least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) - 90),
                                abs(least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) - 270)
                            ) < least(
                                abs(least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) - 90),
                                abs(least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) - 270)
                            )
                        then st_intersection(extended_edge_2, extended_edge_3)

                        when least(
                                abs(least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) - 90),
                                abs(least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) - 270)
                            ) < least(
                                abs(least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) - 90),
                                abs(least(abs(edge_angle_1 - edge_angle_2), 360 - abs(edge_angle_1 - edge_angle_2)) - 270)
                            ) and least(
                                abs(least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) - 90),
                                abs(least(abs(edge_angle_1 - edge_angle_3), 360 - abs(edge_angle_1 - edge_angle_3)) - 270)
                            ) < least(
                                abs(least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) - 90),
                                abs(least(abs(edge_angle_2 - edge_angle_3), 360 - abs(edge_angle_2 - edge_angle_3)) - 270)
                            )
                        then st_intersection(extended_edge_3, extended_edge_1)
                    end;

                    alter table {tjunc}
                    add column corrected_edge_1 geometry,
                    add column corrected_edge_2 geometry,
                    add column corrected_edge_3 geometry;

                    update {tjunc}
                        set 
                            corrected_edge_1 = ST_Makeline(node_corrected, node_outside_1),
                            corrected_edge_2 = ST_Makeline(node_corrected, node_outside_2),
                            corrected_edge_3 = ST_Makeline(node_corrected, node_outside_3);

                    
                       delete from {tjunc} where not st_geometrytype(node_corrected) = 'st_point';
                       delete from {tjunc} where not st_isvalid(node_corrected);
                       

                """
                
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql_query)
        print("Tjunctions identified")
    
        
    def update(self):

        edges = f"{self.all_node_schema}.edge"  
        tjunc = self.village+"."+self.tjunc
        edges_corrected = self.village+"."+self.edges_corrected

        sql_query = f"""
        
             drop table if exists {edges_corrected};
            create table {edges_corrected} as (
                select
                    e.edge_id, e.geom, e.start_node, e.end_node
                from   
                    {edges} e
            );
            alter table {tjunc} add column temp_buffer geometry;
            update {tjunc}
            set 
                temp_buffer = st_buffer(node_geom, 5);

            delete from {edges_corrected}
            using {tjunc}
            where
                st_intersects({edges_corrected}.geom, {tjunc}.temp_buffer);

            alter table {edges_corrected} drop column if exists edge_id;

            insert into {edges_corrected}
            select
                st_makeline(node_corrected, node_outside_1) as geom
            from
                {tjunc}
            where
                node_corrected is not null and node_outside_1 is not null;

            
            insert into {edges_corrected}
            select
                st_makeline(node_corrected, node_outside_2) as geom
            from
                {tjunc}
            where
                node_corrected is not null and node_outside_2 is not null;

            
            insert into {edges_corrected}
            select
                st_makeline(node_corrected, node_outside_3) as geom
            from
                {tjunc}
            where
                node_corrected is not null and node_outside_3 is not null;
            
            
  
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql_query)
        print("Edges updated")

    def filter_polygons(self):
            input = self.village+'.'+self.fp_mid_poly
            output = self.village+'.'+self.fp_mid_poly_filtered
            farmplots = self.village+'.'+self.farmplots
            
            sql_query=f"""
                drop table if exists {output};
                create table {output} as

                select
                    st_multi((st_dump(
                        polygons.geom
                    )).geom) as geom
                from
                    {input} polygons,
                    {farmplots} cp
                where
                    st_intersects(polygons.geom, cp.geom)
                    and
                    st_area(st_intersection(polygons.geom, cp.geom))/st_area(cp.geom) > {self.intersection_thresh}
                    and
                    st_area(cp.geom) > {self.area_thresh}
                ;
                
                alter table {output}
                add column gid serial;
            """
            
            with self.psql_conn.connection().cursor() as curr:
                curr.execute(sql_query)
    def segmentize(self):
        sql_query=f"""
            with edges as (
                select edge_id, start_node, end_node, geom from {self.updated_topo}.edge_data
            ),
            boundary as (
                select
                    (st_dumppoints(st_segmentize(geom, {self.seg_length}))).geom as point
                from
                    edges
            )
            
            select topogeo_addpoint('{self.updated_topo}', point, {self.seg_tol}) from boundary;
        """
        
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql_query)

    def update_trijunctions(self):
        trij = self.village+"."+self.trij
        tjunc = self.village+"."+self.tjunc
        trijunction_table = self.village + "." +self.trijunctions
        sql_query = f"""
            update {trij}
            set
                node_geom = tj.node_corrected
            from 
                {tjunc} tj
            where
                {trij}.node_id = tj.node_id;
            
            update {trijunction_table}
            set 
                geom = tj.node_corrected 
            from
                {tjunc} tj
            where 
                {trijunction_table}.node_id = tj.node_id;
 
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql_query)



    def run(self):
        self.store_trijunction()
        self.update()
        create_topo(self.psql_conn, self.village, self.updated_topo, self.edges_corrected, self.tol, self.srid, 0)
        #polygonize_topo(self.psql_conn, self.village, self.updated_topo, self.fp_mid_poly)
        #self.filter_polygons()
        #create_topo(self.psql_conn, self.village, self.updated_topo, self.fp_mid_poly_filtered, 0, self.srid, 0)
        #self.segmentize()
        self.update_trijunctions()
        print("New updated topo created")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Description for my parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")

    argument = parser.parse_args()
    
    village = argument.village

    tri = t_junction_fixer(village)
    tri.run()


