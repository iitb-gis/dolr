from config import *
from utils import *
import argparse
from collections import Counter
import numpy as np

def analyse_gat_plots(confg, psql_conn):
    gat = confg.setup_details['data']['gat_map_table']
    gat_area_table = confg.setup_details['data']['gat_area_table']
    village = confg.setup_details['setup']['village']
    gat_no = confg.setup_details['val']['gat_no_label']
    
    if config.DEBUG_MODE:
        print("\n----------GAT PLOTS----------")
    if check_table_exists(psql_conn,village,gat):
        if config.DEBUG_MODE:
            print("Gat plots table exists!")
    else:
        print("Gat plots table does not exist! \n TODO: Verify the gat input table.")
        return
    
    sql = f"""
        update {village}.{gat}
        set geom = st_makevalid(geom);
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
    
    sql = f'''
        select
            ({gat_no})
        from
            {village}.{gat};    
    '''
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
        res = curr.fetchall()
    gat_numbers = []
    for result in res:
        gat_numbers.append(str(result[0]))

    if config.DEBUG_MODE:
        print("Total number of gat plots:", len(gat_numbers))
        print("List of repeating gat numbers:",end=" ")
    
    element_counts = Counter(gat_numbers)
    
    # repeated_elements = [element for element, count in element_counts.items() if count > 1]
    frequencies = {element: count for element, count in element_counts.items() if count > 1}
    
    if(len(frequencies) == 0):
        if config.DEBUG_MODE:
            print("No survey number was repeated")
    else:
        print("\ngat no    Number of times repeated")
        for i in frequencies:
            print(i, "          ", frequencies[i])

    print('TODO: Verify the table for the above entries of gat number')
    print("Non integer gat numbers:", end=" ")

    acutal_gat_no_list = []
    if(len(gat_numbers) == 0):
        if config.DEBUG_MODE:
            print("No gat was present")
    else:
        for i in gat_numbers:
            if i is not None:
                if  not i.isnumeric():
                    print(i, end="  ")
                else:
                    acutal_gat_no_list.append(i)
            else:
                print("None",end=' ')
                
    print('\nTODO: Verify the gat map for the above gat numbers')

    if not check_table_exists(psql_conn, village, gat_area_table):
        return
    
    print("\nList of gat numbers present in area table but missing in gat map table:",end=" ")
    sql = f'''
        select 
            ({gat_no})
        from 
            {village}.{gat_area_table};
    '''
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
        res = curr.fetchall()
    ror_list = []
    for result in res:
        ror_list.append(str(result[0]))
    
    for i in range(1,len(ror_list)+1):
        if i not in acutal_gat_no_list and i in ror_list :
            print(i,end='  ')

    print("\n List of gat numbers present in Gat map table but missing in the area table:",end=" ") 
    for i in range(1,len(acutal_gat_no_list)+1):
        if i not in ror_list and i in acutal_gat_no_list :
            print(i,end='  ')
            
    intersections = list_overlaps(psql_conn, village, gat, f'{gat_no}')
    intersections = np.array(intersections)
    filtered_intersections = intersections[intersections[:, 2].astype(float) > 1]
    if config.DEBUG_MODE:
        print("\n\nTotal number of pairs of intersecting polygons:-",len(intersections))
        print("Listing all intersecting survey_no in format (polygon1, polygon2, intersection_area)")
        print(intersections)
    else:
        print("\n\nTotal number of pairs of intersecting polygons with considerable intersection area:-",len(filtered_intersections))
        print("Listing all intersecting survey_no in format (polygon1, polygon2, intersection_area)")
        print(filtered_intersections)
    
if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")
    
    argument = parser.parse_args()
    village = argument.village
        
    config = Config()
    pgconn = PGConn(config)
    
    if village!="":
        config.setup_details['setup']['village'] = village
    
    analyse_gat_plots(config, pgconn)