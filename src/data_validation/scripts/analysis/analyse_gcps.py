from config import *
from utils import *
import argparse
import pandas as pd
import re


def analyse_gcps(confg, psql_conn):
    delimiter = confg.setup_details['val']['label_delimiter']
    schema = confg.setup_details['setup']['village']
    gcp = confg.setup_details['data']['gcp_table']
    gcp_label_column = confg.setup_details['georef']['gcp_label_column']
    gcp_table = schema + "." + gcp
    gcp_report = confg.setup_details['georef']['gcp_report']
    gcp_report_table = schema + "." + gcp_report
    
    if config.DEBUG_MODE:
        print("\n----------GCP----------")
    if check_table_exists(psql_conn,schema,gcp):
        if config.DEBUG_MODE:
            print("GCP table exists!")
    else:
        print("GCP table does not exist! \n TODO: Verify the GCP table input")
        return
    
    if not check_column_exists(psql_conn, schema , gcp, gcp_label_column ):
        confg.setup_details['georef']['gcp_label_toggle']= "False"
    else:
        confg.setup_details['georef']['gcp_label_toggle']= "True"
        sql = f'''
            drop table if exists {gcp_report_table};
            create table {gcp_report_table} as
            select gid, {gcp_label_column} from {gcp_table};
        '''
        with psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        sql = f'''
            alter table {gcp_report_table}
            add column if not exists Parseable varchar(100); 
        '''
        with psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        sql = f''' select {gcp_label_column} from {gcp_table}; '''
        with psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            gcp_labels = curr.fetchall()
        for gcp_label in gcp_labels:
            if gcp_label[0] is None:
                continue
            survey_nos = re.split(delimiter,gcp_label[0])
            flag = "Yes"
            for survey_no in survey_nos:
                try:
                    a = int(survey_no)
                    flag = "Yes"
                except:
                    if survey_no in ['vb' , 'rv', 'rd', 'g']:
                        flag = "Yes"
                    else:
                        flag = "No"
            sql = f'''
                update {gcp_report_table}
                set Parseable = '{flag}'
                where {gcp_label_column} = '{gcp_label[0]}' ;
            '''
            with psql_conn.connection().cursor() as curr:
                curr.execute(sql)
        



if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")
    
    argument = parser.parse_args()
    village = argument.village
        
    config = Config()
    pgconn = PGConn(config)
    
    if village!="":
        config.setup_details['setup']['village'] = village
    
    analyse_gcps(config, pgconn)
