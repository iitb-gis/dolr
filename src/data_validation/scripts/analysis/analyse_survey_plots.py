from config import *
from utils import *
import argparse
from collections import Counter
import numpy as np

def analyse_survey_plots(confg, psql_conn):
    survey = confg.setup_details['data']['survey_map_table']
    akarbandh = confg.setup_details['data']['akarbandh_table']
    village = confg.setup_details['setup']['village']
    survey_no = confg.setup_details['val']['survey_no_label']
    
    if config.DEBUG_MODE:
        print("\n----------SURVEY PLOTS----------")
    if check_table_exists(psql_conn,village,survey):
        if config.DEBUG_MODE:
            print("Survey plots table exists!")
    else:
        print("Survey plots table does not exist! \n TODO: Verify the inputs.")
        return
    
    sql = f"""
        update {village}.{survey}
        set geom = st_makevalid(geom);
    """
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
    
    sql = f'''
        select
            ({survey_no})
        from
            {village}.{survey};    
    '''
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
        res = curr.fetchall()
    survey_numbers = []
    for result in res:
        survey_numbers.append(str(result[0]))

    if config.DEBUG_MODE:
        print("Total number of survey plots:", len(survey_numbers))
        print("List of repeating survey numbers:",end=" ")
    
    element_counts = Counter(survey_numbers)
    
    # repeated_elements = [element for element, count in element_counts.items() if count > 1]
    frequencies = {element: count for element, count in element_counts.items() if count > 1}
    if(len(frequencies) == 0):
        if config.DEBUG_MODE:
            print("No Survey number was repeated")
    else:
        print("\nSurvey no    Number of times repeated")
        for i in frequencies:
            print(i, "          ", frequencies[i])

    print('TODO: Verify the entries for the above survey numbers')
    print("\n Non integer Survey numbers:", end=" ")

    acutal_survey_no_list = []
    if(len(survey_numbers) == 0):
        if config.DEBUG_MODE:
            print("No Survey number present")
    else:
        for i in survey_numbers:
            if i is not None:
                if  not i.isnumeric() :
                    if i not in ['G', 'S', 'R']:
                        print(i, end="  ")
                else:
                    acutal_survey_no_list.append(i)
            else:
                print("None",end=' ')
                
    print('\nTODO : Verify the table for the above survey numbers')
    if not check_table_exists(psql_conn, village, akarbandh):
        return
    
    
    print("\n List of survey numbers present in akarbandh but missing in survey map table:",end=" ")
    sql = f'''
        select 
            ({survey_no})
        from 
            {village}.{akarbandh};
    '''
    with psql_conn.connection().cursor() as curr:
        curr.execute(sql)
        res = curr.fetchall()
    ror_list = []
    for result in res:
        ror_list.append(str(result[0]))
    
    flag = True
    for i in range(1,len(ror_list)+1):
        if i not in acutal_survey_no_list and i in ror_list :
            flag = False
            print(i,end='  ')
    if flag:
        print('None', end = '   ')
    
    flag = True
    print("\n List of survey numbers present in Survey map table but missing in akarbandh table:",end=" ") 
    for i in range(1,len(acutal_survey_no_list)+1):
        if i not in ror_list and i in acutal_survey_no_list :
            print(i,end='  ')
            flag = False
    if flag : 
        print('None', end = '   ')

    intersections = list_overlaps(psql_conn, village, survey, f'{survey_no}')
    intersections = np.array(intersections)
    if(intersections.ndim == 1):
        filtered_intersections = []
    else:
        filtered_intersections = intersections[intersections[:, 2].astype(float) > 1]
    if config.DEBUG_MODE:
        print("\n\nTotal number of pairs of intersecting polygons:-",len(intersections))
        print("Listing all intersecting survey_no in format (polygon1, polygon2, intersection_area)")
        print(intersections)
    else:
        print("\n\nTotal number of pairs of intersecting polygons with considerable intersection area:-",len(filtered_intersections))
        print("Listing all intersecting survey_no in format (polygon1, polygon2, intersection_area)")
        print(filtered_intersections)
    
if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")
    
    argument = parser.parse_args()
    village = argument.village
        
    config = Config()
    pgconn = PGConn(config)
    
    if village!="":
        config.setup_details['setup']['village'] = village
    
    analyse_survey_plots(config, pgconn)