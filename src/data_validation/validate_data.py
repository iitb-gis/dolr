from config import *
from utils import *
from scripts import *
import argparse

def datavalidation(village = "" , schema = "" , option = ""):
    config = Config()
    
    pgconn = PGConn(config)
    if village != "":    
        config.setup_details['setup']['village'] = village

    if option != "":
        config.setup_details['data']['farmplot_load_option'] = option
    
    if schema != "":
        config.setup_details['data']['farmplot_combined_schema'] = schema
    
    return DataValidationAndPreparation(config,pgconn)

class DataValidationAndPreparation:
    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.farmplots = self.config.setup_details["data"]["farmplots_table"]
        self.cadastrals = self.config.setup_details["data"]["cadastrals_table"]
        self.combined_schema = self.config.setup_details['data']['farmplot_combined_schema']
        self.load_option = self.config.setup_details['data']['farmplot_load_option']
        self.original_farmplots = self.config.setup_details["data"]["original_farmplots_table"]
        self.village = self.config.setup_details['setup']['village']
        self.gat = self.config.setup_details['data']['gat_map_table']
        self.survey = self.config.setup_details['data']['survey_map_table']

    def analyse(self):
        # Prepare analysis summary: what is missing, what is duplicate, non-alphanumeric, etc
        
        analyse_survey_plots(self.config,self.psql_conn)
        analyse_cadastrals(self.config,self.psql_conn)
        analyse_gcps(self.config,self.psql_conn)
        analyse_akarbandh(self.config,self.psql_conn)
        if check_table_exists(self.psql_conn, self.village, self.gat):
            analyse_gat_plots(self.config,self.psql_conn)

    def clean_data(self):
        # wrapper for clean data
        cleaner = Data_Cleaner(self.config,self.psql_conn)
        cleaner.run()

    def prepare_survey_map(self):
        # wrapper for correct data
        survey_map_prep = Survey_Map_Processer(self.config,self.psql_conn)
        survey_map_prep.run()
        
    def prepare_gat_map(self):
        # wrapper for gat data
        gat_map_prep = Survey_Map_Processer(self.config,self.psql_conn,gat=True)
        gat_map_prep.run()

    def clean_farmplots(self):   
        farmplot_cleaner = Farmplot_Cleaner(self.config, self.psql_conn )
        farmplot_cleaner.run()

    def run(self):
        self.clean_data()
        self.analyse()
        if check_table_exists(self.psql_conn, self.village, self.survey):
            self.prepare_survey_map()
        if check_table_exists(self.psql_conn, self.village, self.gat):
            self.prepare_gat_map()
        self.clean_farmplots()
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Description for my parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")
    parser.add_argument("-s", "--combinedschema", help="schema containing combined farmplots",
                        required=False, default="")
    parser.add_argument("-o", "--farmplot_toggle", help="0 for village wise farmplots, 1 for combined schema",
                        required=False, default="")


    argument = parser.parse_args()
    
    village = argument.village
    schema = argument.combinedschema
    option = argument.farmplot_toggle
    datacleaner = datavalidation(village , schema , option)
    datacleaner.run()
