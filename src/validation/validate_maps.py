from utils import *
from config import *
from scripts import *
import argparse

def validation(village = ""):
    config = Config()
    
    pgconn = PGConn(config)
    if village != "":    
        config.setup_details['setup']['village'] = village
    
    return Validation(config,pgconn)

class Validation:
    def __init__(self,config,psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.village = self.config.setup_details['setup']['village']
        self.survey_georef = config.setup_details['data']['survey_georeferenced_table']
        self.before_removal = self.config.setup_details['data']['original_before_removal']
        self.shifted_faces = self.config.setup_details['data']['shifted_faces_table']
        
    def report(self):
        # generates the report table in the village schema
        add_report(self.config, self.psql_conn)
    
    def summary(self):
        # add the summary of the village to the summary table (common across all villages)
        add_summary(self.config, self.psql_conn)
    
    def setup_validation(self):
        # add stats to current maps to make validation easier
        sv = Setup_Validate(self.config, self.psql_conn)
        sv.run()
    
    def restore_interior(self):
        restore = interior_plots_refit(self.village)
        restore.run(self.survey_georef, self.survey_georef, self.survey_georef + self.before_removal)
        restore.run(self.shifted_faces, self.shifted_faces, None)
        
    def run(self):

        self.restore_interior()

        print("Setting up validation")
        self.setup_validation()
        print("Adding report")
        self.report()
        print("Adding summary")
        self.summary()
    
if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for my parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")

    argument = parser.parse_args()
    
    village = argument.village
    
    validate = validation(village)
    validate.run()
    