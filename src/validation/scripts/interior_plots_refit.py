from utils import *
from config import *
import argparse, ast

def interior_plots_refit(village):
    config = Config()
    if village != "":
        config.setup_details['setup']['village'] = village
    pgconn = PGConn(config)
    
    return Interior_plots_refit(config,pgconn)

class Interior_plots_refit:
    def __init__(self,config,psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        
        self.survey_original = self.config.setup_details['data']['survey_map_table']
        self.schema_name = self.config.setup_details['setup']['village']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        self.cadastrals = self.config.setup_details['data']['cadastrals_table']        
        self.survey_georef = config.setup_details['data']['survey_georeferenced_table']
        self.before_removal = self.config.setup_details['data']['original_before_removal']
        self.interior_plots_with_parent = self.config.setup_details['data']['interior_plots_with_parent']
        self.survey_processed = self.config.setup_details['data']['survey_processed']

    def run(self, source, destination, original_source):
        interior_plots_with_parent = self.interior_plots_with_parent
        if original_source == None:
            original_source = self.survey_processed

        # step 1 - create copy of source
        source_cp = source + "_cp"
        sql = f'''
            DROP TABLE IF EXISTS {self.schema_name}.{source_cp};
            CREATE TABLE {self.schema_name}.{source_cp} AS 
            SELECT * from {self.schema_name}.{source}
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

        #step 2
        # create a table with the current geometries of outer polygons having the newly transformed coordinates.
        current_position_ext = "current_position_ext"
        sql = f'''
            DROP TABLE IF EXISTS {self.schema_name}.{current_position_ext};
            CREATE TABLE {self.schema_name}.{current_position_ext} AS 
            SELECT int_pwp.*, 
                src.geom AS ext_current_geom
            FROM {self.schema_name}.{interior_plots_with_parent} AS int_pwp
            JOIN {self.schema_name}.{source} AS src ON int_pwp.ext_survey_no = src.survey_no;
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

        #step 3
        # create a table with the current geometries of outer polygons having the newly transformed coordinates. i.e vector from ext geom to current ext geom
        shifted_centroid_ext = "shifted_centroid_ext"
        sql = f'''
            DROP TABLE IF EXISTS {self.schema_name}.{shifted_centroid_ext};
            CREATE TABLE {self.schema_name}.{shifted_centroid_ext} AS 
            select *, st_point(st_x(st_centroid(ext_current_geom)) - st_x(st_centroid(ext_geom)) , st_y(st_centroid(ext_current_geom)) - st_y(st_centroid(ext_geom))) as shifted_ext
            from {self.schema_name}.{current_position_ext}
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        
        # step 4 - now we need to shift int geom according to the current centroids of int
        shifted_internals = "shifted_internals"
        sql = f'''
            DROP TABLE IF EXISTS {self.schema_name}.{shifted_internals};
            CREATE TABLE {self.schema_name}.{shifted_internals} AS 
            select *, st_translate(int_geom, st_x(shifted_ext), st_y(shifted_ext)) as new_shifted_int
            from {self.schema_name}.{shifted_centroid_ext}
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            
        # step 5 - scaling up interior-geometry to current scale
        shifted_and_scaled_internals = "shifted_and_scaled_internals"
        sql = f'''
            DROP TABLE IF EXISTS {self.schema_name}.{shifted_and_scaled_internals};
            CREATE TABLE {self.schema_name}.{shifted_and_scaled_internals} AS 
            select *, st_scale(new_shifted_int,st_makePoint(sqrt(st_area(ext_current_geom)/st_area(ext_geom)), sqrt(st_area(ext_current_geom)/st_area(ext_geom))), st_centroid(ext_current_geom)) as scaled_int
            from {self.schema_name}.{shifted_internals}
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

        # step 6 - remove interior plots from original geom now..
            # copy paste same georef
        final_map = "final_map"
        sql = f'''
            DROP TABLE IF EXISTS {self.schema_name}.{final_map};
            CREATE TABLE {self.schema_name}.{final_map} AS 
            SELECT * from {self.schema_name}.{original_source}
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

            # update outside geoms..
        sql = f'''
            update {self.schema_name}.{final_map} as final_map
            set geom = cp_map.geom
            from {self.schema_name}.{source_cp} as cp_map
            where cp_map.survey_no = final_map.survey_no
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

            # update inside geoms..
        sql = f'''
            update {self.schema_name}.{final_map} as final_map
            set geom = (
                select st_multi(st_collect(scaled_int)) from {self.schema_name}.{shifted_and_scaled_internals} where int_survey_no = final_map.survey_no group by int_survey_no
            )
            from {self.schema_name}.{shifted_and_scaled_internals} as new_map
            where new_map.int_survey_no = final_map.survey_no
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            
            # delete interior plots from parent plots
        sql = f'''
            update {self.schema_name}.{final_map} as final_map
            set geom = st_multi(st_difference(final_map.geom,
            (
                select st_multi(st_collect(scaled_int)) from {self.schema_name}.{shifted_and_scaled_internals} where ext_survey_no = final_map.survey_no  group by ext_survey_no
            )))
            from {self.schema_name}.{shifted_and_scaled_internals} as new_map
            where new_map.ext_survey_no = final_map.survey_no
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

        # step 7 - copy to destination
        sql = f'''
            DROP TABLE IF EXISTS {self.schema_name}.{destination};
            CREATE TABLE {self.schema_name}.{destination} AS 
            SELECT * from {self.schema_name}.{final_map};
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    argument = parser.parse_args()
    village = argument.village
    refit = interior_plots_refit(village)
    refit.delete_inside_polygons()
