from config import *
from utils import *
import argparse

def setup_validate(village = ""):
    config = Config()
    
    pgconn = PGConn(config)
    if village != "":    
        config.setup_details['setup']['village'] = village
    
    return Setup_Validate(config,pgconn)

class Setup_Validate:
    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.village = config.setup_details['setup']['village']
        self.survey_no_label = config.setup_details['val']['survey_no_label']
        self.survey_georef = config.setup_details['data']['survey_georeferenced_table']
        self.shifted = config.setup_details['data']['shifted_faces_table']
        self.jitter_spline = config.setup_details['data']['jitter_spline_table']
        self.fp = config.setup_details['data']['farmplots_table']
        self.intersection_thresh = config.setup_details['val']['intersection_thresh']
        self.akarbandh_col = config.setup_details['data']['survey_map_akarbandh_col']
        self.actual_area_col = config.setup_details['val']['actual_area_column']
        self.fr_method = config.setup_details['val']['farm_rating_method']
    
    def add_stats(self, schema, table, farmplots):
        add_varp(self.psql_conn, schema, table, 'varp')
        add_shape_index(self.psql_conn, schema, table, 'shape_index')
        add_farm_rating(self.psql_conn, schema, table, farmplots, 'farm_rating',self.fr_method)
        add_farm_intersection(self.psql_conn, schema, table, farmplots, 'farm_intersection')
        add_excess_area(self.psql_conn, schema, table, farmplots, 'excess_area')
            
    def validate_georeferenced(self, georef_table, akarbandh_area_col):
        sql = f"""
            alter table {georef_table}
            add column if not exists akarbandh_area_diff float;

            update {georef_table} a
            set akarbandh_area_diff = ((st_area(geom)/10000)-{akarbandh_area_col})/{akarbandh_area_col}
            where 
                {akarbandh_area_col} is not null
                and 
                {akarbandh_area_col} > 0;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
        
    def validate_with_georeferenced(self, table, georef_table, common_col, akarbandh_area_col, actual_area_col):
        sql = f"""
            alter table {table}
            add column if not exists akarbandh_area_diff float,
            add column if not exists area_diff float,
            add column if not exists perimeter_diff float,
            add column if not exists deviation float,
            add column if not exists actual_area_diff float;
            
            with translated_georef as (
                select 
                    georef.{common_col} as {common_col},
                    georef.{akarbandh_area_col} as {akarbandh_area_col},
                    georef.{actual_area_col} as {actual_area_col},
                    st_translate(
                        georef.geom,
                        st_x(st_centroid(t.geom)) - st_x(st_centroid(georef.geom)),
                        st_y(st_centroid(t.geom)) - st_y(st_centroid(georef.geom))
                    ) as geom 
                from 
                    {georef_table} as georef, {table} as t
                where 
                    georef.valid = true
                    and
                    georef.{common_col} = t.{common_col}
            )
            update {table} as t
            set akarbandh_area_diff = ((st_area(t.geom)/10000) - subquery.{akarbandh_area_col})/subquery.{akarbandh_area_col},
                area_diff = (st_area(t.geom) - st_area(subquery.geom))/st_area(subquery.geom),
                perimeter_diff = (st_perimeter(t.geom) - st_perimeter(subquery.geom))/st_perimeter(subquery.geom),
                deviation = (st_area(st_difference(t.geom, subquery.geom)) + st_area(st_difference(subquery.geom, t.geom)))/(2*st_area(subquery.geom)),
                actual_area_diff = (st_area(t.geom) - subquery.{actual_area_col})/subquery.{actual_area_col}
            from
                translated_georef as subquery
            where
                t.{common_col} = subquery.{common_col}
                and
                subquery.{akarbandh_area_col} > 0;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
              
    def run(self):
        print(f"Validating for village {self.village}")
        
        print("Adding stats to survey georeferenced table")
        self.add_stats(self.village, self.survey_georef, self.fp)
        
        print("Adding survey number to georeferenced table")
        add_survey_no(self.psql_conn, self.village, self.shifted, self.survey_georef, self.survey_no_label, self.intersection_thresh)
        add_survey_no(self.psql_conn, self.village, self.jitter_spline, self.survey_georef, self.survey_no_label, self.intersection_thresh)
        
        print("Adding stats to shifted faces table")
        self.add_stats(self.village, self.shifted, self.fp)
        self.add_stats(self.village, self.jitter_spline, self.fp)
        
        print("Validating survey georeferenced table")
        self.validate_georeferenced(self.village+"."+self.survey_georef, self.akarbandh_col)
        
        print("Validating shifted faces table")
        self.validate_with_georeferenced(self.village+"."+self.shifted, self.village+"."+self.survey_georef, self.survey_no_label, self.akarbandh_col, self.actual_area_col)
        self.validate_with_georeferenced(self.village+"."+self.jitter_spline, self.village+"."+self.survey_georef, self.survey_no_label, self.akarbandh_col, self.actual_area_col)
            
if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for my parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")

    argument = parser.parse_args()
    
    village = argument.village
    
    sv = setup_validate()
    sv.run()