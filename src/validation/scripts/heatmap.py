import geopandas as gpd
import os, argparse
from config import *
from utils import *
import matplotlib.pyplot as plt
from matplotlib.patches import Patch


config = Config()
psql_conn = PGConn(config)

village = config.setup_details['setup']['village']

    
def assign_color_farmrating(value):
    value = value * 100
    if value >= 90:
        return '#1a9641'
    elif value >= 80:
        return '#ffffc0'
    else:
        return '#d7191c'
    
def assign_legend_farmrating():
    legend_labels = ['90 - 100', '80 - 90', '<80']
    legend_colors = ['#1a9641', '#ffffc0','#d7191c']
    return legend_labels, legend_colors
    
def assign_color_area_diff(value):
    value = value * 100
    if value <= -5:
        return '#ca0020'
    elif value <= -3:
        return '#ec846e'
    elif value <= 0:
        return '#f6d6c8'
    elif value <= 3:
        return '#cfe3ed'
    elif value <= 5:
        return '#76b4d5'
    else:
        return '#0571b0'
    
def assign_legend_area_diff():
    legend_labels = ['< -5', '-5 - -3', '-3 - 0', '0 - 3', '3 - 5', '> 5']
    legend_colors = ['#ca0020', '#ec846e','#f6d6c8','#cfe3ed','#76b4d5','#0571b0']
    return legend_labels, legend_colors
    
def assign_color_deviation(value):
    value = value * 100
    if value >= 5:
        return '#d7191c'
    elif value >= 3:
        return '#ffffc0'
    else:
        return '#1a9641'
    
def assign_legend_deviation():
    legend_labels = ['0 - 3', '3 - 5', '>5']
    legend_colors = ['#1a9641', '#ffffc0','#d7191c']
    return legend_labels, legend_colors


def createplots(psql_conn, village , map, path):
    sql = f'''
            select * from {village}.{map};
        '''
    gdf = gpd.GeoDataFrame.from_postgis(sql, psql_conn.connection(), geom_col='geom')
    columns = ['farm_rating' , 'actual_area_diff' , 'deviation']
    methods = ['assign_color_farmrating', 'assign_color_area_diff', 'assign_color_deviation']
    legends = ['assign_legend_farmrating', 'assign_legend_area_diff', 'assign_legend_deviation']
    for column, method_name, legend_name in zip(columns,methods, legends):
        method = globals()[method_name]
        legend = globals()[legend_name]
        gdf['color'] = gdf[column].apply(method)
        # Plot the GeoDataFrame
        fig, ax = plt.subplots(figsize=(10, 8))
        ax.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)
        gdf.plot(ax=ax, color=gdf['color'], edgecolor='black')
        legend_labels , legend_colors = legend()
        # Create legend with color boxes and descriptions
        legend_patches = [Patch(color=color, label=label) for color, label in zip(legend_colors, legend_labels)]
        plt.legend(handles=legend_patches, loc='upper left' , bbox_to_anchor=(1, 1))
        plt.suptitle(map)
        plt.title(column)
        plt.savefig(f"{path}/{column}.png")
        plt.close()


def completevillage(village, path):
    maps = ['shifted_faces', 'jitter_spline_output_regularised_05']
    for map in maps:
        if not os.path.exists(f"{path}/{village}/{map}"):
            os.makedirs(f"{path}/{village}/{map}")
        createplots(psql_conn, village, map, f"{path}/{village}/{map}")

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for my parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=True, default="")
    parser.add_argument("-p", "--path", help="storage path",
                        required=True, default="")

    argument = parser.parse_args()
    
    village = argument.village
    path = argument.path
    
    completevillage(village,path)