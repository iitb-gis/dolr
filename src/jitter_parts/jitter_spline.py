from utils import *
import argparse
from config import *
from scripts import *

def splinejitter(village):
    config = Config()
    if village != "":
        config.setup_details['setup']['village'] = village
    psql_conn = PGConn(config)
    return SplineJitter(config, psql_conn)


class SplineJitter:

    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn  = psql_conn
        self.jitter_nodes = self.config.setup_details['local_jitter']['jitter_nodes']
        self.village = self.config.setup_details['setup']['village']
        self.georeferenced_topo_schema = self.village + "_georeferenced_topo"
        self.nodes_map = self.config.setup_details['local_jitter']['nodes_map']
        self.output_nodes_table = self.config.setup_details['local_jitter']['nodes_tranformed_spline']
        self.survey_georeferenced = self.config.setup_details['data']['survey_georeferenced_table']
        self.jitter_spline_output = self.config.setup_details['local_jitter']['jitter_spline_output']

    def create_node_map(self, map_table):
        
        sql_query = f"""
            drop table if exists {self.village}.{map_table};
            create table {self.village}.{map_table} as

            select
                original_nodes.node_id,
                jitter_nodes.geom as geom
            from
                {self.georeferenced_topo_schema}.node original_nodes,
                {self.village}.jitter_nodes jitter_nodes
            where
                original_nodes.node_id = jitter_nodes.node_id
            ;
        """

        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql_query)



    def run(self):
        obj = Local_Jitter(self.psql_conn, self.config )
        obj.run()
        self.create_node_map(self.nodes_map)
        transform_points(self.psql_conn, self.georeferenced_topo_schema+".node" , self.village+"."+self.nodes_map, self.village+"."+self.output_nodes_table, method = "spline" )
        get_updated_faces_from_nodes(self.psql_conn, self.georeferenced_topo_schema, self.village, self.village+"."+self.survey_georeferenced, self.village+"."+self.jitter_spline_output,self.village+"."+ self.output_nodes_table)


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    argument = parser.parse_args()
    village = argument.village
    obj = splinejitter(village)
    obj.run()
        