from utils import *
import time

class Local_Jitter:

    def __init__(self, psql_conn, config):
        self.psql_conn = psql_conn
        self.config = config
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        self.georeferenced_faces = self.config.setup_details['local_jitter']['georeferenced_faces']
        self.village = self.config.setup_details['setup']['village']
        self.georeferenced_topo_schema = self.village + "_georeferenced_topo"
        self.survey_georeferenced = self.config.setup_details['data']['survey_georeferenced_table']
        self.jitter_polygons = self.config.setup_details['local_jitter']['jitter_polygons']
        self.temp_jitter_polygons = self.config.setup_details['local_jitter']['temp_jitter_polygons']
        self.jitter_nodes = self.config.setup_details['local_jitter']['jitter_nodes']
        self.cur_mapped_polygon = self.config.setup_details['local_jitter']['cur_mapped_polygon']
        self.vb_toggle = self.config.setup_details['local_jitter']['vb_toggle']
        self.village_boundary = self.config.setup_details['local_jitter']['village_boundary']
        self.lamb = float(self.config.setup_details['local_jitter']['lambda'])

    def check_node_jitter(self, node_id):
        sql_query="""
            select * from {schema}.{jitter_nodes} where node_id={node_id};
        """.format(schema=self.village, node_id=node_id, jitter_nodes = self.jitter_nodes)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
            jitter_node=curs.fetchone()
        if jitter_node is not None:
            return True
        else:
            return False


    def make_faces(self):
        sql_query="""
            select polygonize('{topo_schema}');

            drop table if exists {schema}.{original_face_table};
            create table {schema}.{original_face_table} as
            
            select
                face_id,
                st_makevalid(st_getfacegeometry('{topo_schema}', face_id)) as geom
            from
                {topo_schema}.face
            where
                face_id>0
                and
                st_perimeter(st_makevalid(st_getfacegeometry('{topo_schema}', face_id))) * 
                    st_perimeter(st_makevalid(st_getfacegeometry('{topo_schema}', face_id))) /
                    st_area(st_makevalid(st_getfacegeometry('{topo_schema}', face_id))) < 55
            ;
        """.format(schema=self.village, topo_schema=self.georeferenced_topo_schema, original_face_table = self.georeferenced_faces )

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
        
        sql_query = """
            update {schema}.{original_face_table}
            set geom = st_multi(geom);
            
            alter table {schema}.{original_face_table}
            add column if not exists survey_no varchar(100) default '',
            add column if not exists akarbandh_area float,
            add column if not exists valid bool,
            add column if not exists farm_intersection float,
            add column if not exists farm_rating float;
            
            update {schema}.{original_face_table} o
            set survey_no = a.survey_no,
                akarbandh_area = a.akarbandh_area,
                valid = a.valid
            from {schema}.{input_partition} as a
            where st_area(st_intersection(o.geom,a.geom))/st_area(o.geom) > 0.8;

            CREATE INDEX idx_original_faces_jitter_geom ON {schema}.{original_face_table} USING GIST(geom);
            
        """.format(schema=self.village , input_partition=self.survey_georeferenced, original_face_table = self.georeferenced_faces)
        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)



    def setup_local_jitter(self):
        sql_query = f"""
            drop table if exists {self.village}.{self.jitter_polygons};
            create table {self.village}.{self.jitter_polygons} (
                face_id int,
                farm_rating float,
                geom Geometry
            );

            drop table if exists {self.village}.{self.temp_jitter_polygons};
            create table {self.village}.{self.temp_jitter_polygons} (
                face_id int,
                farm_rating float,
                geom Geometry
            );
        """

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)

        
    def get_area(self, schema, input):
        input_table = schema + "." + input
        sql_query = f"""
            select st_area(geom) as area from {input_table} polygon;
        """

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
            area_fetch = curs.fetchone()
        
        if area_fetch is not None:
            cur_area = area_fetch[0]
        else:
            cur_area = 0
        
        return cur_area

    
    def get_farm_intersection(self, schema, input, reference):
        input_table = schema + "." + input
        reference_table = schema + "." + reference
        sql_query = f"""
            with farm_plots as (
                select
                    st_collect(farm_plots.geom) as geom
                from
                    {input_table} polygon,
                    {reference_table} farm_plots
                where
                    st_intersects(farm_plots.geom, polygon.geom)
            )

            select
                st_area(st_intersection(polygon.geom, farm_plots.geom))/
                 st_area(polygon.geom) as int_ratio
            from
                {input_table} polygon,
                farm_plots
            ;
        """

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
            farm_intersection_fetch = curs.fetchone()
        
        if farm_intersection_fetch is not None and farm_intersection_fetch[0] is not None:
            cur_farm_intersection = farm_intersection_fetch[0]
        else:
            cur_farm_intersection = 0

        return cur_farm_intersection

    def survey_plot_jitter(self, face_id):
        sql_query = """
            drop table if exists {schema}.cur_survey_polygon;
            create table {schema}.cur_survey_polygon as

            select
                face_id,
                geom
            from
                {schema}.{original_faces_jitter}
            where
                face_id = {face_id}
            ;
        """.format(schema=self.village, face_id=face_id , original_faces_jitter= self.georeferenced_faces)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)

        bnds = ((-0.0001, 0.0001), (0.9999, 1.0001), (0.9999, 1.0001), (-25, 25), (-25, 25))

        result = minimize(farm_rating_params, [0, 1, 1, 0, 0], args=(self.psql_conn,
            self.village, "cur_survey_polygon", "temp_farm_rating" , self.farmplots , self.lamb), bounds=bnds, tol=0.00001)
        
        trans_params = result.x
        delta_x = trans_params[3]
        delta_y = trans_params[4]
        print([delta_x , delta_y])
        sql_query = '''
            drop table if exists {schema}.{mapped_polygon};
            create table {schema}.{mapped_polygon} as

            select
            
                original_faces_jitter.face_id as face_id,
                st_translate(geom, {delta_x},{delta_y}) as geom
            from
                {schema}.{original_face_table} original_faces_jitter
            where
                face_id = {id}
            ;
        '''.format(schema=self.village, delta_x=delta_x,
            delta_y=delta_y, id=face_id, original_face_table = self.georeferenced_faces, mapped_polygon = self.cur_mapped_polygon)
        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
        
        self.cur_farm_rating = -1* farm_rating_params([0,1,1,0,0], self.psql_conn, self.village, self.cur_mapped_polygon, "temp_farm_rating", self.farmplots , 0)

        sql_query = '''
            insert into {schema}.{temp_jitter_polygons}
            select face_id, {farm_rating}, geom from {schema}.{mapped_polygon};
        '''.format(schema=self.village, farm_rating=self.cur_farm_rating, temp_jitter_polygons= self.temp_jitter_polygons, mapped_polygon = self.cur_mapped_polygon)
        
        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
   
        # add_farm_intersection(self.psql_conn, self.village, self.cur_mapped_polygon, self.farmplots, 'farm_intersection')
        # sql = f'''
        #         select farm_intersection from {self.village}.{self.cur_mapped_polygon};
        # '''
        # with self.psql_conn.connection().cursor() as curr:
        #     curr.execute(sql)
        #     self.cur_farm_intersection = curr.fetchone()
        # self.cur_farm_intersection = self.cur_farm_intersection[0]
        self.cur_farm_intersection = self.get_farm_intersection(self.village, self.cur_mapped_polygon, self.farmplots)    
        self.cur_area = self.get_area(self.village, self.cur_mapped_polygon)
        print([self.cur_farm_rating, self.cur_farm_intersection, self.cur_area])
        if self.cur_farm_rating < self.farm_rating_threshold_jitter:
            return ["Red", delta_x, delta_y]
        
        if self.cur_farm_intersection < self.farm_intersection_threshold_jitter:
            print(f"Too much void area for face {face_id}!")
            return ["Red", delta_x, delta_y]
        
        if self.cur_area < self.area_threshold_jitter:
            print(f"Too small for face {face_id}!")
            return ["Red", delta_x, delta_y]
                
        sql_query = '''
            insert into {schema}.{jitter_polygons}
            select face_id, {farm_rating}, geom from {schema}.{mapped_polygon};
        '''.format(schema=self.village, delta_x=delta_x,
            farm_rating=self.cur_farm_rating, delta_y=delta_y, id=face_id, jitter_polygons= self.jitter_polygons , mapped_polygon = self.cur_mapped_polygon)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)


        self.trans_nodes_original(face_id, trans_params)
        #a = input("Enter to continue")
        return ["Green", delta_x, delta_y]
    





    def method2(self, face_id , delta_x, delta_y, flag):
        sql_query = """
            with original_faces_jitter as (
                select
                    face_id,
                    geom
                from
                    {schema}.{original_face_table}
                where
                    face_id = '{face_id}'
            ),
            original_nodes as (
                select
                    nodes.node_id as node_id,
                    nodes.geom as geom
                from
                    {topo_schema}.node nodes,
                    original_faces_jitter
                where
                    st_intersects(st_buffer(original_faces_jitter.geom, 0.5), nodes.geom)
            )

            select
                node_id
            from
                original_nodes
            ;
        """.format(schema=self.village, topo_schema=self.georeferenced_topo_schema, original_face_table = self.georeferenced_faces,
                face_id=face_id)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
            nodes_fetch = curs.fetchall()
        
        original_nodes = [node[0] for node in nodes_fetch]
        for node_id in original_nodes:
            sql = f"""
                insert into {self.village}.delta_translations (node_id, face_id,  delta_x, delta_y, flag)
                values ({node_id}, {face_id}, {delta_x}, {delta_y}, '{flag}');
            """
            with self.psql_conn.connection().cursor() as curr:
                curr.execute(sql)
            
















    def trans_nodes_original(self, face_id, trans_params):
        delta_x = trans_params[3]
        delta_y = trans_params[4]

        sql_query = """
            with original_faces_jitter as (
                select
                    face_id,
                    geom
                from
                    {schema}.{original_face_table}
                where
                    face_id = '{face_id}'
            ),
            original_nodes as (
                select
                    nodes.node_id as node_id,
                    nodes.geom as geom
                from
                    {topo_schema}.node nodes,
                    original_faces_jitter
                where
                    st_intersects(st_buffer(original_faces_jitter.geom, 0.5), nodes.geom)
            )

            select
                node_id
            from
                original_nodes
            ;
        """.format(schema=self.village, topo_schema=self.georeferenced_topo_schema, original_face_table = self.georeferenced_faces,
                face_id=face_id)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
            nodes_fetch = curs.fetchall()
        
        original_nodes = [node[0] for node in nodes_fetch]

        for node_id in original_nodes:
            if self.check_node_jitter(node_id):
                continue
            sql_query = """
                insert into {schema}.{jitter_nodes}

                select
                    node_id,
                    st_translate(geom, {delta_x}, {delta_y}) as geom
                from
                    {topo_schema}.node
                where
                    node_id = {node_id}
                ;
            """.format(schema=self.village, topo_schema=self.georeferenced_topo_schema,
                node_id=node_id, delta_x=delta_x, delta_y=delta_y, jitter_nodes= self.jitter_nodes)
            
            with self.psql_conn.connection().cursor() as curs:
                curs.execute(sql_query)


    def fix_local_jitter(self):
        
        sql_query = """
            select face_id from {village}.{original_face_table};
        """.format(original_face_table = self.georeferenced_faces , village=self.village)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
            face_ids_fetch = curs.fetchall()
        face_ids = [face[0] for face in face_ids_fetch]
        face_ids.sort()
        for face_id in face_ids:
            vb_intersect = False
            if self.vb_toggle == 'True':
                sql = f'''
                    with cur_face as (
                    select 
                        face_id,
                        geom
                    from 
                        {self.village}.{self.georeferenced_faces}
                    where 
                        face_id = {face_id}
                    )
                    select 
                        st_intersects( st_buffer(a.geom,1) , b.geom )
                    from
                        cur_face a,
                        {self.village}.{self.village_boundary} b
                    ;
                '''
                with self.psql_conn.connection().cursor() as curr:
                    curr.execute(sql)
                    vb_intersect = curr.fetchone()[0]
            print(face_id)
            if vb_intersect == True:
                continue
            sql_query = f"""
                with cov_buf as (
                    select
                        st_buffer(st_union(geom), 25) as geom
                    from
                        {self.village}.{self.jitter_polygons}
                )

                select
                    st_area(st_intersection(cov_buf.geom, cur_face.geom)) as area
                from
                    cov_buf,
                    {self.village}.{self.georeferenced_faces} cur_face
                where
                    cur_face.face_id = {face_id}
                ;
            """
            
            with self.psql_conn.connection().cursor() as curs:
                curs.execute(sql_query)
                area_fetch = curs.fetchone()
            
            if area_fetch[0] is not None and area_fetch[0] > 0.1:
                continue

            print(area_fetch)

            res = self.survey_plot_jitter(face_id)
            self.method2(face_id, res[1], res[2], res[0])
        
    
    def create_table_delta(self, schema, table_name):
        sql = f'''
            drop table if exists {schema}.{table_name};
            create table {schema}.{table_name}(
            node_id integer,
            face_id integer,
            delta_x float,
            delta_y float,
            flag text
            );
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)


    def run(self):
        sql_query="""
            drop table if exists {schema}.{jitter_nodes};
            create table {schema}.{jitter_nodes} (
                node_id integer,
                geom geometry(Point, 32643)
            );
        """.format(schema=self.village, jitter_nodes=self.jitter_nodes)

        with self.psql_conn.connection().cursor() as curs:
            curs.execute(sql_query)
        
        create_topo(self.psql_conn, self.village, self.georeferenced_topo_schema, self.survey_georeferenced, 0.005)
        self.make_faces()
        self.create_table_delta(self.village, "delta_translations")
        self.setup_local_jitter()
        if self.vb_toggle == 'True':
            add_village_boundary(self.psql_conn, self.village, self.survey_georeferenced, 'survey_no', 'vb')
            sql = f'''
                drop table if exists {self.village}.{self.village_boundary};
                create table {self.village}.{self.village_boundary} as
                select * from {self.village}.{self.survey_georeferenced} where survey_no = 'vb' ;
            '''
            with self.psql_conn.connection().cursor() as curr:
                curr.execute(sql)
            remove_village_boundary(self.psql_conn, self.village, self.survey_georeferenced, 'survey_no', 'vb')

        print("------LOCAL JITTER 1------")
        time.sleep(1)
        self.area_threshold_jitter = 15000
        self.farm_rating_threshold_jitter = 0.97
        self.farm_intersection_threshold_jitter = 0.5
        self.fix_local_jitter()

        print("------LOCAL JITTER 2------")
        time.sleep(1)
        self.farm_rating_threshold_jitter = 0.95
        self.fix_local_jitter()
        if self.vb_toggle == "True":
            sql = f'''
                with boundary_nodes as(
                    select
                        a.node_id,
                        a.geom
                    from
                        {self.georeferenced_topo_schema}.node as a,
                        {self.village}.{self.village_boundary} as b
                    where
                        st_intersects(st_buffer(a.geom,1), b.geom)
                )
                insert into {self.village}.{self.jitter_nodes}
                select
                    c.node_id,
                    c.geom
                from
                    boundary_nodes as c
                ;    
            '''
            with self.psql_conn.connection().cursor() as curr:
                curr.execute(sql)


    