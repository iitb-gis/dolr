import os
import subprocess
import argparse
from utils import *
from config import *


def run_field_validation(village, inp , path):
    config = Config()
    if village != "":
        config.setup_details['setup']['village'] = village

    if inp == "0":
        config.setup_details['field_validation']['input_table'] = config.setup_details['data']['shifted_faces_table']
    elif inp == "1":
        config.setup_details['field_validation']['input_table'] = config.setup_details['local_jitter']['jitter_spline_output']
    elif inp == "2":
        config.setup_details['field_validation']['input_table'] = config.setup_details['data']['survey_georeferenced_table']


    if path != "":
        config.setup_details['field_validation']['validation_points_path'] = path
    pgconn = PGConn(config)
    
    return Field_validation(config,pgconn)
    


class Field_validation:

    def __init__(self, config, psql_conn):
        self.psql_conn = psql_conn
        self.config = config
        self.schema = self.config.setup_details['field_validation']['schema']
        self.field_validation_points = self.config.setup_details['field_validation']['validation_points']
        self.path = self.config.setup_details['field_validation']['validation_points_path']
        self.village = self.config.setup_details['setup']['village']
        self.input = self.config.setup_details['field_validation']['input_table']
        self.validation_points_map = self.config.setup_details['field_validation']['validation_points_map']
        self.output = self.config.setup_details['field_validation']['field_validation_output']
        self.summary = self.config.setup_details['field_validation']['summary']

    def load_points(self, config, psql_conn, path_to_points):
        srid = config.setup_details['setup']['srid']
        flag = check_schema_exists(self.psql_conn, self.schema)
        if not flag:
            create_schema(self.psql_conn, self.schema)
        for root, dirs, files in os.walk(path_to_points, topdown=True):
            for file in files:
                file_location = os.path.join(root, file)
                table_name = self.field_validation_points
                if file.endswith(".shp"):
                    ogrinfo_cmd = [
                        'ogrinfo',
                        '-q',
                        file_location
                    ]
                    output = subprocess.check_output(ogrinfo_cmd, universal_newlines=True)
                    if 'Point' in output:
                        ogr2ogr_cmd = [
                            'ogr2ogr','-f','PostgreSQL','-t_srs',f'EPSG:{srid}',
                            'PG:dbname=' + psql_conn.details["database"] + ' host=' +
                                psql_conn.details["host"] + ' user=' + psql_conn.details["user"] +
                                ' password=' + psql_conn.details["password"],
                            file_location,
                            '-lco', 'OVERWRITE=YES',
                            '-lco', 'GEOMETRY_NAME=geom',
                            '-lco', 'schema=' + config.setup_details['field_validation']['schema'], 
                            '-lco', 'SPATIAL_INDEX=GIST',
                            '-lco', 'FID=gid',
                            '-dim', '2',
                            '-nln', table_name,
                            '-unsetFieldWidth'
                        ]
                        subprocess.run(ogr2ogr_cmd)

    def process_points(self  ):
        sql = f'''
                alter table {self.schema}.{self.field_validation_points}
                drop column if exists gid;
                alter table {self.schema}.{self.field_validation_points}
                add gid serial;
            '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

        sql = f'''
                drop table if exists {self.village}.{self.field_validation_points};
                create table {self.village}.{self.field_validation_points} as
                with outer_boundary as(
                    select st_buffer(st_union(geom),10) as geom
                    from {self.village}.{self.input}
                ) 
                select 
                    a.gid as gid,
                    a.geom as geom
                from 
                    {self.schema}.{self.field_validation_points} as a,
                    outer_boundary as b
                where
                    st_contains(b.geom, a.geom)
                ;
            '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)


    def field_validation(self):
        topo_schema = self.village+"_"+self.input+ "_topo" 
        create_topo(self.psql_conn, self.village, topo_schema, self.input)
        corner_nodes = self.input + "_corner_nodes"
        get_corner_nodes(self.psql_conn, topo_schema, self.village, corner_nodes )
        create_distance_gcp_map(self.psql_conn, self.village, corner_nodes, self.field_validation_points, self.validation_points_map, check_distance_thresh=False )
        sql = f'''
                drop table if exists {self.village}.{self.output};
                create table {self.village}.{self.output} as
                with good_plots as(
                    select st_collect(geom) as geom from {self.village}.{self.input} where farm_rating > 0.92
                )
                select
                    a.gid as field_validation_id,
                    a.gcp_geom as gcp_geom,
                    a.node_id as output_point_id,
                    a.node_geom as output_point_geom, 
                    st_x(a.gcp_geom) as field_validation_latitude,
                    st_y(a.gcp_geom) as field_validation_longitude,
                    st_x(a.node_geom) as output_point_latitude,
                    st_y(a.node_geom) as output_point_longitude,
                    st_distance(a.gcp_geom, a.node_geom) as distance,
                    st_distance(a.node_geom, b.geom) as distance_from_good
                from
                    {self.village}.{self.validation_points_map} as a,
                    good_plots as b
                ;
            '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

    def add_villagestats(self):
        
        sql = f'''
                drop table if exists {self.schema}.{self.summary};
                create table {self.schema}.{self.summary}(
                id serial,
                village varchar(100),
                total_points int,
                points_0_3 int,
                points_3_7 int,
                points_7_15 int,
                points_15_greater int
                );            
            '''
        # with self.psql_conn.connection().cursor() as curr:
        #     curr.execute(sql)
        sql = f'''
                select
                    count(*)
                from
                    {self.village}.{self.field_validation_points}
                ;
            '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            result = curr.fetchall()
        total_points = result[0][0]
        sql = f'''
                select
                    count( case when distance between 0 and 3 then 1 end ),
                    count( case when distance between 3 and 7 then 1 end ),
                    count( case when distance between 7 and 15 then 1 end ),
                    count( case when distance > 15 then 1 end )
                from
                    {self.village}.{self.output}
                ;
            '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            result = curr.fetchall()
        p_0_3 = result[0][0]
        p_3_7 = result[0][1]
        p_7_15 = result[0][2]
        p_15_greater = result[0][3]
        sql = f'''
                delete from {self.schema}.{self.summary}
                where village = '{self.village}';
                insert into {self.schema}.{self.summary} (village , total_points, points_0_3, points_3_7, points_7_15,  points_15_greater )
                values( '{self.village}', {total_points} , {p_0_3} , {p_3_7}, {p_7_15}, {p_15_greater} );
            '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)


    def run(self):
        self.load_points(self.config, self.psql_conn, self.path)
        self.process_points()
        self.field_validation()
        self.add_villagestats()


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    parser.add_argument("-i", "--input", help="0 for facefit map, 1 for jitter spline, 2 for survey georeferenced",
                        required=False, default="")
    parser.add_argument("-p", "--path", help="Path to field validation points",
                        required=False, default="")
    argument = parser.parse_args()
    village = argument.village
    inp = argument.input
    path = argument.path
    field_validation = run_field_validation(village, inp , path)
    field_validation.run()