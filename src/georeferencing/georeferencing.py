from utils import *
from scripts import *
from src.data_validation.scripts.analysis.analyse_gcps import *
from itertools import combinations
from config import *


def georeferencer(village, gcp_label_toggle):
    config = Config()
    if village != "":
        config.setup_details['setup']['village'] = village
    if gcp_label_toggle != "":
        config.setup_details['georef']['gcp_label_toggle'] = gcp_label_toggle
    pgconn = PGConn(config)
    
    return Georeferencer(config,pgconn)

class Georeferencer:
    def __init__(self,config,psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.survey_jitter =  self.config.setup_details['data']['survey_jitter_table']
        self.schema_name = self.config.setup_details['setup']['village']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        self.gcp = self.config.setup_details['data']['gcp_table']
        self.survey_georeferenced = self.config.setup_details['data']['survey_georeferenced_table']
        self.akarbandh_col = self.config.setup_details['data']['survey_map_akarbandh_col']
        self.actual_area = self.config.setup_details['georef']['actual_area_column']
        
        self.survey_processed = self.config.setup_details['data']['survey_processed']
        self.gcp_map = self.config.setup_details['georef']['gcp_map']
        self.gcp_report = self.config.setup_details['georef']['gcp_report']
        self.final_gcps_used = []
        self.final_output = [""]
        self.gcp_label_toggle = self.config.setup_details['georef']['gcp_label_toggle'] == "True"
        self.survey_shifted = self.config.setup_details['georef']['survey_shifted']
        self.temp_georef_schema = self.schema_name+self.config.setup_details['georef']['temp_georeferencing_schema']
        self.temp_georef_topo_schema = self.schema_name+self.config.setup_details['georef']['temp_georeferencing_topo_schema']
        self.srid = self.config.setup_details['setup']['srid']
        self.tol = self.config.setup_details['georef']['topo_tol']
        self.nodes_table_suffix = self.config.setup_details['georef']['nodes_table_suffix']
        self.trijunctions_table_suffix = self.config.setup_details['georef']['trijunctions_suffix']
        self.temp_mapped_nodes = self.config.setup_details['georef']['temp_mapped_nodes']
        self.temp_output_nodes = self.config.setup_details['georef']['temp_output_nodes']
        
        self.akarbandh_validity_thresh = self.config.setup_details['val']['akarbandh_validity_thresh']
        self.heatmap_path = self.config.setup_details['val']['path_heat']
        self.fr_method = config.setup_details['val']['farm_rating_method']

        
    def get_gcp_map(self, topo_schema, input_schema, input_table, gcp_table, nodes_table, gcp_map, check_distance_thresh = True):
        get_corner_nodes(self.psql_conn, topo_schema, input_schema, 
                         nodes_table, only_trijunctions=True)
        create_node_labels(self.psql_conn, input_schema, input_table, nodes_table, village_boundary_label='vb')
        create_gcp_map(self.psql_conn, input_schema, nodes_table, self.gcp, self.gcp_map, use_labels = self.gcp_label_toggle, check_distance_thresh = check_distance_thresh, jitter = True)
        add_gcp_label(self.psql_conn, input_schema, nodes_table, gcp_table, gcp_map)

    def georef(self):
        
        create_schema(self.psql_conn, self.temp_georef_schema, True)
        
        input_table = self.survey_shifted if self.gcp_label_toggle else self.survey_jitter
        check_distance_thresh = False if self.gcp_label_toggle else False
        trijunctions_table = input_table+self.trijunctions_table_suffix
        nodes_table = input_table+self.nodes_table_suffix
        
        copy_table(self.psql_conn, self.schema_name+'.'+input_table, self.schema_name+'.temp_'+input_table)
        
        create_topo(self.psql_conn, self.schema_name, self.temp_georef_topo_schema, f'temp_{input_table}',
                    self.tol, self.srid)
        
        if config.DEBUG_MODE:
            a = input("Press Enter to continue: ")
        
        self.get_gcp_map(self.temp_georef_topo_schema, self.schema_name, input_table, self.gcp, trijunctions_table, self.gcp_map, check_distance_thresh = check_distance_thresh)
        
        if config.DEBUG_MODE:
            a = input("Press Enter to continue: ")
        
        sql = f"""
            drop table if exists {self.schema_name}.{nodes_table};
            create table {self.schema_name}.{nodes_table} as 
            select 
                node_id,
                geom
            from
                {self.temp_georef_topo_schema}.node
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            
        method_eval = []
        
        for method in [('polynomial',1),('polynomial',2),('spline',), ('spline4',), ('spline3',)]:
            m = method[0]
            degree = method[1] if len(method)>1 else ""
            
            print(f"\n-----RUNNING {m}{degree}-----")
            
            evaluation = self.georef_using_gcps(m,
                                   f"{self.schema_name}.temp_{input_table}", 
                                   f"{self.schema_name}.{nodes_table}",
                                   f"{self.schema_name}.{self.gcp_map}",
                                   degree)
            if evaluation == None:
                continue
            method_best_table = f'{self.schema_name}.{self.survey_georeferenced}_{m}{degree}'
            copy_table(self.psql_conn, evaluation['table_name'], method_best_table)
            evaluation['table_name']=method_best_table
            evaluation['method']=f'{m}{degree}'
            method_eval.append(evaluation)
            
            print(f"Best metrics are {evaluation}")
        
        excess_area = excess_area_without_parameters(self.psql_conn, self.schema_name, 
                                                         self.survey_jitter, self.farmplots, self.schema_name)
        distortion = get_distortion(self.psql_conn, self.schema_name, self.survey_jitter,
                                    self.survey_processed, self.schema_name)
        
        method_eval.append({'method': 'jitter',
                            'gcp_gids': [],
                            'table_name': self.schema_name+'.'+self.survey_jitter,
                            'excess_area': excess_area,
                            'distortion': distortion})      
        
        method_eval = list(filter(lambda x: x['distortion'] < 1, method_eval))
        method_eval.sort(key=lambda x: x['excess_area'])
        
        if len(method_eval) == 0:
            return None
        
        copy_table(self.psql_conn, method_eval[0]['table_name'], 
                   self.schema_name+'.'+self.survey_georeferenced)
        validate_geom(self.psql_conn, self.schema_name, self.survey_georeferenced)

        print(f"\n Final method used is {method_eval[0]['method']}")
        
    
    def georef_using_gcps(self, method, input_table, input_nodes_table, gcp_map, degree=""):
        
        sql = f"""
            select gid from {gcp_map};
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
            gcp_gids = [i[0] for i in curr.fetchall()]
            num_gcps = len(gcp_gids)
        if method == 'polynomial':
            num_used_gcps = int(max(num_gcps-2, (degree+2)*(degree+1)/2))
        elif method == 'projective':
            num_used_gcps = max(num_gcps-2, 4)
        elif method == 'spline':
            num_used_gcps = max(num_gcps-2, 4)
        elif method == 'spline2':
            num_used_gcps = max(num_gcps-2, 4)
        elif method == 'spline3':
            num_used_gcps = max(num_gcps-2, 4)
        elif method == 'spline4':
            num_used_gcps = max(num_gcps-2, 4)
        else:
            num_used_gcps = num_gcps
        
        if(num_used_gcps > num_gcps):
            return None
        metrics = []
        
        for i, comb in enumerate(list(combinations(gcp_gids, num_used_gcps))+[tuple(gcp_gids)]):
            try:
                output_table_name = f'survey_{method}{degree}_{i}'
                
                sql = f"""
                    drop table if exists {self.schema_name}.{self.temp_mapped_nodes};
                    create table {self.schema_name}.{self.temp_mapped_nodes} as 
                    select
                        node_id as node_id,
                        gcp_geom as geom
                    from
                        {gcp_map}
                    where
                        gid in {tuple(comb)}
                    ;
                """
                with self.psql_conn.connection().cursor() as curr:
                    curr.execute(sql)
                    
                transform_points(self.psql_conn, input_nodes_table, self.schema_name+'.'+self.temp_mapped_nodes,
                                self.schema_name+'.'+self.temp_output_nodes, method, self.srid, degree)
                get_updated_faces_from_nodes(self.psql_conn, self.temp_georef_topo_schema,
                                            self.schema_name, input_table, self.temp_georef_schema+'.'+output_table_name, 
                                            self.schema_name+'.'+self.temp_output_nodes)
                excess_area = excess_area_without_parameters(self.psql_conn, self.temp_georef_schema, 
                                                            output_table_name, self.farmplots, self.schema_name)
                distortion = get_distortion(self.psql_conn, self.temp_georef_schema, output_table_name,
                                            self.survey_processed, self.schema_name)
                eval_dict = {'index': i,
                                'gcp_gids': comb,
                                'table_name': self.temp_georef_schema+'.'+output_table_name,
                                'excess_area': excess_area,
                                'distortion': distortion}
                metrics.append(eval_dict)
                
                print(eval_dict)
                
                if config.DEBUG_MODE:
                    a = input("Press Enter to continue: ")
            
            except Exception as e:
                print(f"Error in {method}{degree} {i}")
                print(e)
                continue
        
        metrics = list(filter(lambda x: x['distortion'] < 1, metrics))
        metrics.sort(key=lambda x: x['excess_area'])
        if len(metrics) == 0:
            return None
        else:
            return metrics[0]

    def report(self):
        flag = check_column_exists(self.psql_conn, self.schema_name, self.gcp_report, "Parseable/Non_Parseable")
        if flag==False:
            analyse_gcps(self.config, self.psql_conn )
        gcp_trijunction_match(self.config, self.psql_conn)
    
    def remove_interiors(self):
        remover = remove_interior_plots(self.schema_name)
        remover.run(self.survey_georeferenced, self.survey_georeferenced)
    
    def run(self):
        # fix_gaps(self.psql_conn, self.schema_name, self.survey_processed)
        if check_table_exists(self.psql_conn, self.schema_name, self.gcp):
            self.georef()
            self.report()
        else:
            copy_table(self.psql_conn, self.schema_name+'.'+self.survey_jitter, self.schema_name+'.'+self.survey_georeferenced)
            validate_geom(self.psql_conn, self.schema_name, self.survey_georeferenced)
            print(f"\n Final method used is jitter")
            
        self.add_akarbandh_area_diff(self.schema_name + "." + self.survey_georeferenced, 'akarbandh_area')
        self.insert_actual_area()
        self.setup_validate()
        create_heatmaps(self.psql_conn, self.schema_name, self.heatmap_path, self.survey_georeferenced, 'akarbandh_area_diff', [3, 5], ['#008000', '#FFFF00', '#FF0000'])
        
        print("----------Now removing interior plots from georef map---------------")
        self.remove_interiors()
        print("----------Removed interior plots and wrote back to self.georeferenced_table----------")
        
    def setup_validate(self):
        print("----Setting up validation----")
        add_varp(self.psql_conn, self.schema_name, self.survey_georeferenced, 'varp')
        add_farm_intersection(self.psql_conn, self.schema_name, self.survey_georeferenced, self.farmplots, 'farm_intersection')
        add_farm_rating(self.psql_conn, self.schema_name, self.survey_georeferenced, self.farmplots, 'farm_rating', self.fr_method)
        add_shape_index(self.psql_conn, self.schema_name, self.survey_georeferenced, 'shape_index')

    def add_akarbandh_area_diff(self, georef_table, akarbandh_area_col):
        sql = f"""
            alter table {georef_table}
            add column if not exists akarbandh_area_diff float;

            update {georef_table} a
            set akarbandh_area_diff = ((st_area(geom)/10000)-{akarbandh_area_col})/{akarbandh_area_col}
            where 
                {akarbandh_area_col} is not null
                and 
                {akarbandh_area_col} > 0;
        """
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

    def insert_actual_area(self):
        add_column(self.psql_conn, self.schema_name+"."+self.survey_georeferenced, self.actual_area, "float")
        query1 = f"""
            UPDATE {self.schema_name}.{self.survey_georeferenced}
            SET {self.actual_area} = CASE
                                WHEN (ABS({self.akarbandh_col}*10000 - ST_Area(geom)) / ST_Area(geom))*100 <= {self.akarbandh_validity_thresh}
                                THEN {self.akarbandh_col} * 10000
                                ELSE ST_Area(geom)
                            END;

        """

        query2 = f"""
            UPDATE {self.schema_name}.{self.survey_georeferenced}
            SET {self.actual_area} = ST_Area(geom);
        """

        try:
            if(check_column_exists(self.psql_conn, self.schema_name, self.survey_georeferenced, self.akarbandh_col)):
                print("Adding mix of akarbandh_area and polygon area in actual_area")
                with self.psql_conn.connection().cursor() as curr:
                        curr.execute(query1)
            else:
                print("Adding area of polygon in actual_area")
                with self.psql_conn.connection().cursor() as curr:
                        curr.execute(query2)
                        
        except Exception as e:
            print("Error in inserting column actual_area")
            print(e)
        
    
if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    parser.add_argument("-g", "--gcp_label_toggle", help="GCP label column exists?",
                        required=False, default="")
    argument = parser.parse_args()
    gcp_toggle = argument.gcp_label_toggle
    village = argument.village
    georef = georeferencer(village , gcp_toggle)
    georef.run()
    