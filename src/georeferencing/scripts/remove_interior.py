from utils import *
from config import *
import argparse, ast

def remove_interior_plots(village):
    config = Config()
    if village != "":
        config.setup_details['setup']['village'] = village
    pgconn = PGConn(config)
    
    return Removing_interior(config,pgconn)

class Removing_interior:
    def __init__(self,config,psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        
        self.survey_original = self.config.setup_details['data']['survey_map_table']
        self.schema_name = self.config.setup_details['setup']['village']
        self.farmplots = self.config.setup_details['data']['farmplots_table']
        self.cadastrals = self.config.setup_details['data']['cadastrals_table']
        self.before_removal = self.config.setup_details['data']['original_before_removal']
        self.survey_processed = self.config.setup_details['data']['survey_processed']
        self.interior_plots_with_parent = self.config.setup_details['data']['interior_plots_with_parent']

    def run(self, source, destination):
        
        #copy source to source copy
        source_cp = source + self.before_removal
        sql = f'''
            drop table if exists {self.schema_name}.{source_cp};
            create table {self.schema_name}.{source_cp} as 
                select * from {self.schema_name}.{source};
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)     
        
        # step 1 - take the polgonised exterior rings alone of the plots
        polygonised = "polygonised_exterior"

        sql = f'''
            drop table if exists {self.schema_name}.{polygonised};
            create table {self.schema_name}.{polygonised} as 
                select gid, st_makepolygon(st_exteriorring((st_dump(geom)).geom)) as geom, survey_no from {self.schema_name}.{source};
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

        #step 2 - remove interior plots and store them seperately to remove_interior
        remove_interior = "remove_interior"
        sql = f'''
            drop table if exists {self.schema_name}.{remove_interior};
            create table {self.schema_name}.{remove_interior} as 
                select * from {self.schema_name}.{polygonised} as inner_poly
                where (
                    select count(case when st_within(inner_poly.geom, geom) then 1 else null end) from {self.schema_name}.{polygonised} where inner_poly.survey_no != survey_no
                ) = 0;
        '''
        # print(sql)
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)
      
        #step 3 - store just the interior plots
        interior_plots = "interior_plots"
        sql = f'''
            drop table if exists {self.schema_name}.{interior_plots};
            create table {self.schema_name}.{interior_plots} as 
                select * from {self.schema_name}.{polygonised} as inner_poly
                where (
                    select count(case when st_within(inner_poly.geom, geom) then 1 else null end) from {self.schema_name}.{polygonised} where inner_poly.survey_no != survey_no
                ) > 0;
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

        #step 4 - stores interior plots with their corresponding parents
        interior_plots_with_parent = self.interior_plots_with_parent
        sql = f'''
            drop table if exists {self.schema_name}.{interior_plots_with_parent};
            create table {self.schema_name}.{interior_plots_with_parent} as 
            (
                select int_poly.survey_no as int_survey_no, int_poly.geom as int_geom, ext_poly.survey_no as ext_survey_no, ext_poly.geom as ext_geom

                from {self.schema_name}.{interior_plots} as int_poly
                join {self.schema_name}.{remove_interior} as ext_poly
                on st_within(int_poly.geom, ext_poly.geom)
            )
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)


        # step 5 - multipolygonise the exterior rings
        multipolygonised = "multipolygonised"

        sql = f'''
        drop table if exists {self.schema_name}.{multipolygonised};
        create table {self.schema_name}.{multipolygonised} as
            
        select survey_no, st_multi(st_collect(geom)) as geom
        from {self.schema_name}.{remove_interior}
        group by survey_no
        '''
        # print(sql)
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql) 

        # step 6 - put into self.survey_processed

        sql = f'''
        drop table if exists {self.schema_name}.{destination};
        create table {self.schema_name}.{destination} as
        select * from {self.schema_name}.{source_cp};
        
        update {self.schema_name}.{destination} as processed_map
        set geom = new_map.geom
        from {self.schema_name}.{multipolygonised} as new_map
        where new_map.survey_no = processed_map.survey_no;
        
        delete from {self.schema_name}.{destination} as t_1
        using {self.schema_name}.{interior_plots_with_parent} as t_2
        where t_1.survey_no = t_2.int_survey_no;
        '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql) 


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    argument = parser.parse_args()
    village = argument.village
    remover = remove_interior_plots(village)
    remover.run()