#!/usr/bin/bash

villages=()

for folder in "$1"/*; do
    if [ -d "$folder" ]; then
        folderName=$(basename "$folder")
        IFS='_' read -ra ADDR <<< "$folderName"
        vlg=($(echo "${ADDR[1]}" | awk '{print tolower($0)}'))
        villages+=$(echo "$vlg" | tr -d ' ')
    fi
done


base_path="src"

echo "Running data loading for $1 taluka "
python3 "$base_path/data_loading/load_data.py -p $1 -t 1"

for village in "${villages[@]}"; do
    echo "Processing $village..."

    # Run each Python script with the current village as an argument
    echo "Running data validation for $village..."
    python3 "$base_path/data_validation/validate_data.py" -s combined_farmplots -o 1 -v "$village"

    echo "Creating farm graph for $village..."
    python3 "$base_path/farm_graph/create_graph.py" -v "$village"

    echo "Applying jitter for $village..."
    python3 "$base_path/Jitter/jitter.py" -v "$village"

    echo "Georeferencing for $village..."
    python3 "$base_path/georeferencing/georeferencing.py" -v "$village"
    
    echo "Finished processing $village."
    echo "--------------------------------"
done

# {
# start_time=`date +%s`


# python3 src/data_validation/validate_data.py -v $village

# python3 src/farm_graph/create_graph.py -v $village

# python3 src/Jitter/jitter.py -v $village
# python3 src/georeferencing/georeferencing.py -v $village
# python3 src/jitter_parts/jitter_spline.py -v $village
# python3 src/face_fit/refine_map.py -v $village



# python3 src/validation/validate_maps.py -v $village

# echo run time is $(($(date +%s)-$start_time)) s

# } > logs/$village.log 2>&1