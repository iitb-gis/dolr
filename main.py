from config import *
from utils import *
from src import *
import argparse

def main(village=""):
    config = Config()
    
    pgconn = PGConn(config)
    if village != "":    
        config.setup_details['setup']['village'] = village
    
    return Main(config, pgconn)

class Main:
    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        
    def run_data_loading(self):
        dl = DataLoading(self.config, self.psql_conn)
        dl.run()
        
        fdl = FarmplotLoading(self.config, self.psql_conn)
        fdl.run()
    
    def run_data_validation(self):
        dv = DataValidationAndPreparation(self.config, self.psql_conn)
        dv.run()
    
    def run_farmplot_processing(self):
        fp = Farm_Graph(self.config, self.psql_conn)    
        fp.run()
    
    def run_georeferencing(self):
        georef = Georeferencer(self.config, self.psql_conn)
        georef.run()
    
    def run_face_fit(self):
        ff = Face_Fit(self.config, self.psql_conn)
        ff.run()
    
    def run_possession(self):
        pos = Possession(self.config, self.psql_conn)
        pos.run()
    
    def run_validation(self):
        val = Validation(self.config, self.psql_conn)
        val.run()
    
    def run(self):
        # self.run_data_loading()
        self.run_data_validation()
        self.run_farmplot_processing()
        self.run_georeferencing()
        self.run_face_fit()
        self.run_possession()
        self.run_validation()

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for my parser")

    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")

    argument = parser.parse_args()
    
    village = argument.village
    main_pipeline = main(village)
    main_pipeline.run()
    
    
    