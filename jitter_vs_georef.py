
from utils import *
from config import *
import argparse


def jittervsgeoref(village = ""):
    config = Config()
    if village != "":
        config.setup_details["setup"]["village"] = village
    
    pgconn = PGConn(config)
    
    return JittervsGeoref(config,pgconn)


class JittervsGeoref:

    def __init__(self, config, psql_conn):
        self.config = config
        self.psql_conn = psql_conn
        self.village = self.config.setup_details['setup']['village']
        self.georef = self.config.setup_details['data']['survey_georeferenced_table']
        self.jitter = self.config.setup_details['data']['survey_jitter_table']
        self.schema = "field_validation"
        self.output = "jitter_vs_georef"

    def add_metrics(self):
        if not check_table_exists(self.psql_conn, self.schema, self.output):
            sql = f''' create table {self.schema}.{self.output} 
                            (
                                village varchar(50),
                                jitter_xmin float, 
                                jitter_ymin float,
                                jitter_centroidx float,
                                jitter_centroidy float,
                                georef_xmin float, 
                                georef_ymin float,
                                georef_centroidx float,
                                georef_centroidy float,
                                corner_distance float, 
                                centroid_distance float
                            );
                    '''
            with self.psql_conn.connection().cursor() as curr:
                curr.execute(sql)
        
        sql = f'''
                delete from {self.schema}.{self.output}
                where village = '{self.village}' ;
                with comparison as (
                    select 
                        st_xmin(st_union(a.geom)) as jitter_xmin,
                        st_ymin(st_union(a.geom)) as jitter_ymin,
                        st_x(st_centroid(st_union(a.geom))) as jitter_centroidx,
                        st_y(st_centroid(st_union(a.geom))) as jitter_centroidy,
                        st_xmin(st_union(b.geom)) as georef_xmin,
                        st_ymin(st_union(b.geom)) as georef_ymin,
                        st_x(st_centroid(st_union(b.geom))) as georef_centroidx,
                        st_y(st_centroid(st_union(b.geom))) as georef_centroidy, 
                        sqrt((st_xmin(st_union(a.geom))-st_xmin(st_union(b.geom)))*(st_xmin(st_union(a.geom))-st_xmin(st_union(b.geom))) + (st_ymin(st_union(a.geom))-st_ymin(st_union(b.geom)))*(st_ymin(st_union(a.geom))-st_ymin(st_union(b.geom))) ) as corner_distance,
                        st_distance(st_centroid(st_union(b.geom)), st_centroid(st_union(a.geom))) as centroid_distance
                    from
                        {self.village}.{self.jitter} as a,
                        {self.village}.{self.georef} as b
                )
                insert into {self.schema}.{self.output} 
                            (
                                village,
                                jitter_xmin , 
                                jitter_ymin ,
                                jitter_centroidx,
                                jitter_centroidy ,
                                georef_xmin , 
                                georef_ymin ,
                                georef_centroidx ,
                                georef_centroidy ,
                                corner_distance , 
                                centroid_distance )
                select '{self.village}' ,
                                c.jitter_xmin , 
                                c.jitter_ymin ,
                                c.jitter_centroidx,
                                c.jitter_centroidy ,
                                c.georef_xmin , 
                                c.georef_ymin ,
                                c.georef_centroidx ,
                                c.georef_centroidy ,
                                c.corner_distance , 
                                c.centroid_distance 
                from comparison as c;
            '''
        with self.psql_conn.connection().cursor() as curr:
            curr.execute(sql)

    def run(self):
        
        self.add_metrics()
        

if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-v", "--village", help="Village",
                        required=False, default="")
    argument = parser.parse_args()
    village = argument.village

    obj = jittervsgeoref(village)
    obj.run()
    

    