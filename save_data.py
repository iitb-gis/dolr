import subprocess
import os
import argparse
import pandas as pd
from config import *
from utils import PGConn

def save_data(config,path):
    
    village = config.setup_details['setup']['village']
    host = config.setup_details['psql']['host']
    port = config.setup_details['psql']['port']
    database = config.setup_details['psql']['database']
    user = config.setup_details['psql']['user']
    password = config.setup_details['psql']['password']
    survey_jitter = config.setup_details['data']['survey_jitter_table']
    survey_georef = config.setup_details['data']['survey_georeferenced_table']
    #shifted = config.setup_details['data']['shifted_faces_table']
    possession = config.setup_details['data']['possession_table']
    report = config.setup_details['val']['report_table']
    gcp_report = config.setup_details['georef']['gcp_report']
    farmplots = config.setup_details['data']['farmplots_table']
    jitter_spline = config.setup_details['local_jitter']['jitter_spline_output']
    cadastrals = config.setup_details['data']['cadastrals_table']
    field_validation = config.setup_details['field_validation']['field_validation_output']

        
    village_folder_path = os.path.join(path,village)
    if not os.path.exists(village_folder_path):
        os.makedirs(village_folder_path)
        print(f'Creating directory {village_folder_path}')
    for table in [survey_jitter, survey_georef, farmplots, cadastrals, jitter_spline, field_validation]:
        table_folder = os.path.join(village_folder_path, table)
        file = os.path.join(table_folder, f'{village}_{table}.shp')
        
        if not os.path.exists(table_folder):
            os.makedirs(table_folder)
        
        ogr2ogr_cmd = [
            'ogr2ogr','-f','ESRI Shapefile',f'{file}',
            'PG:dbname=' + database + ' host=' + host + ' user=' + user +' password=' + password + ' port=' + port,
            '-sql',
            f'select * FROM {village}.{table}'
        ]
        subprocess.run(ogr2ogr_cmd)
        
    # for table in [report, gcp_report]:
    #     file = os.path.join(village_folder_path, f'{village}_{table}.csv')
    #     psql_conn = PGConn(config)
    #     sql_query = f'''
    #         select * from {village}.{table};
    #     '''
    #     with psql_conn.connection().cursor() as curr:
    #         curr.execute(sql_query)
    #         columns = [d[0] for d in curr.description]
    #         data = curr.fetchall()
    #     df = pd.DataFrame(data,columns = columns)
    #     df.to_csv(file, index=False)
    
    
    zip_file = f'{village_folder_path}.zip'
    if os.path.exists(zip_file):
        os.remove(zip_file)
        zip_file = f'{village_folder_path}.zip'

    zip_cmd = [
        'zip','-r',
        zip_file,
        f'{village}'
    ]  
    subprocess.run(zip_cmd, cwd=path)  

if __name__=="__main__":
    
    parser = argparse.ArgumentParser(description="Description for parser")

    parser.add_argument("-p", "--path", help="Path to folder to save",
                        required=True, default="")
    parser.add_argument("-v", "--village", help="Village name",
                        required=False, default="")
    
    argument = parser.parse_args()
    path = argument.path
    village = argument.village
    
    if path=="":
        print("ERROR, no data storing path")
        exit()
        
    config = Config()
    villages = [village]
    
    villages=["dagdagad", "waghalgaon", "gopa", "deolanakh", "deolanabk", "matargaon", "khatnapur", "shekhapur", "kharburdi" ]

    # villages = ['akoli','antarweli','badwani','banpimpla','dhangarmoha','dongargaonshelgaon','dongarpimpla','harangul','khadgaon','mankadevi','padegaon','ukhlikh','umlanaiktanda']
    for vil in villages:
        if vil!="":
            config.setup_details['setup']['village'] = vil
            
        save_data(config, path)
    
    