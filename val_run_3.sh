#!/bin/bash

villages=()

# List all folders and convert their names to lowercase, then store in the array
for folder in "$1"/*; do
    lowercase_name=$(basename "$folder" | tr '[:upper:]' '[:lower:]')
    villages+=("$lowercase_name")
done

# List of villages
villages=("dagdagad" "waghalgaon" "gopa" "deolanakh" "deolanabk" "matargaon" "khatnapur" "shekhapur" "kharburdi"  )

# Loop through each village and run the desired command
for village in "${villages[@]}"
do
    echo "Running village: $village"
    # Add your command to run the village here
    # For example: ./run_village.sh $village
    python3 src/field_validation/validate.py -v $village -i 1 -p /mnt/d/Modern_land/field_validation
done
